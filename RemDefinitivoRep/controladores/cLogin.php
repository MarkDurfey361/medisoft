<?php

session_start();
include '../conexionBD/conexion.php';

$usuario = filter_input(INPUT_POST, 'usuario');
$contrasena = filter_input(INPUT_POST, 'contrasena');

$seleccionUsuario = "SELECT * FROM usuarios WHERE nombreUsuario= _utf8 '$usuario' COLLATE utf8_bin";

$sentencia = $DBcon->prepare($seleccionUsuario);
$sentencia->execute();

$usuarioObtenido = array();

function InstanciarSesion($isActiva, $usuario, $tipoSesion) {
    $_SESSION['loggedin'] = $isActiva;
    $_SESSION['username'] = $usuario;
    $_SESSION['tipo'] = $tipoSesion;
}

while ($row = $sentencia->fetch(PDO::FETCH_ASSOC)) {
    $usuarioObtenido[] = $row;
}

if (count($usuarioObtenido) == 1) {

    $codificado = json_encode($usuarioObtenido);

    $descifrado = json_decode($codificado, TRUE);

    if ($contrasena == $descifrado[0]["contrasenaUsuario"]) {

        switch ($descifrado[0]["tipoUsuario"]) {
            case "Administrador":
                InstanciarSesion(true, $usuario, $descifrado[0]['tipoUsuario']);
                header('Location: ../vistas/vPanelAdministrador.php');
                break;

            case "Medico":
                InstanciarSesion(true, $usuario, $row['tipoUsuario']);
                header('Location: ../vistas/vPanelMedico.php');
                break;

            case "Recepcionista":
                InstanciarSesion(true, $usuario, $row['tipoUsuario']);
                header('Location: ../vistas/vPanelRecepcionista.php');
                break;
            default:
                echo 'Tipo de usuario no identificado.';
                break;
        }

    }
    
} else {
    echo "Error al iniciar sesion.";
}