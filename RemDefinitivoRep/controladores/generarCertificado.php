<?php

require_once '../PHPWord-master/src/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();

use PhpOffice\PhpWord\TemplateProcessor;

$templateWord = new TemplateProcessor('../Repositorio/plantilla_certificado.docx');
 
$nombre = $_POST['nombre'];
$sexo = $_POST['sexo'];
$fecha_nacimiento = $_POST['fechaNac'];

$chk1 = "";
$chk2 = "";

if($_POST['organica']=="Sí"){
    $chk1 = "X";
}

if($_POST['cronica']=="Sí"){
    $chk2 = "X";
}

$comentarios =$_POST['comentarios'] ;
$grupo_sanguineo = $_POST['gSanguineo'];
$factor_rh = $_POST['rH'];
$fecha_exhibicion = $fecha=strftime("%Y-%m-%d", time());


// --- Asignamos valores a la plantilla
$templateWord->setValue('nombre',$nombre);
$templateWord->setValue('sexo',$sexo);
$templateWord->setValue('fecha_nacimiento',$fecha_nacimiento);
$templateWord->setValue('chk1',$chk1);
$templateWord->setValue('chk2',$chk2);
$templateWord->setValue('comentarios',$comentarios);
$templateWord->setValue('grupo_sanguineo',$grupo_sanguineo);
$templateWord->setValue('factor_rh',$factor_rh);
$templateWord->setValue('fecha_exhibicion',$fecha_exhibicion);

// --- Guardamos el documento
$templateWord->saveAs('../certificados/Certificado-'.str_replace(" ", "_", $nombre).'.docx');

//fopen("..//certificados//Certificado-".utf8_decode(str_replace(" ", "_", $nombre)).'.docx', r);

header("Content-Disposition: attachment; filename=../certificados/Certificado-".str_replace(" ", "_", $nombre).".docx; charset=iso-8859-1");
echo '../certificados/Certificado-'.  utf8_encode(str_replace(" ", "_", $nombre)).'.docx';
