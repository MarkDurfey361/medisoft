<?php

if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_FILES["fileToUpload"]["type"])){
    
    $target_dir = "../HistoriasClinicas/";
$carpeta=$target_dir;
if (!file_exists($carpeta)) {
    mkdir($carpeta, 0777, true);
}

$target_file = $carpeta . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $errors[]= "El archivo es una imagen - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $errors[]= "El archivo no es una imagen.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    $errors[]="Lo sentimos, archivo ya existe.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 3145728) {
    $errors[]= "Lo sentimos, el archivo es demasiado grande.  Tamaño máximo admitido: 3 MB";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "docx" /* && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" */) {
    $errors[]= "Lo sentimos, sólo archivos .docx (archivos de Word) son permitidos.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $errors[]= "Lo sentimos, tu archivo no fue subido.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
       
        $idPaciente = $_POST['idPaciente'];
        $comentarios = $_POST['comentarios'];
        
        $conexion = mysql_connect('localhost', 'root', '');
        mysql_select_db('uni_rem');
        
        mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

        $registrarHistoria = "INSERT INTO historia_clinica (id_paciente, fecha_subida, comentarios, archivo_historia) VALUES('" . $idPaciente . "',curdate(),'" . $comentarios . "','" . $target_file . "')";

        $respuesta = mysql_query($registrarHistoria);

        if ($respuesta == true) {
            $messages[]= "El Archivo ha sido subido correctamente y ha quedado registrado";
        } else {
            $messages[]= "El Archivo ha sido subido correctamente, aunque falló su registro. El archivo será eliminado para reintentar.".$target_file;
            unlink($target_file);
        }

    } else {
       $errors[]= "Lo sentimos, hubo un error subiendo el archivo.";
    }
}

if (isset($errors)){
	?>
	<div class="alert alert-danger alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong>Error!</strong> 
	  <?php
	  foreach ($errors as $error){
		  echo"<p>$error</p>";
	  }
	  ?>
	</div>
	<?php
}

if (isset($messages)){
	?>
	<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  <strong>Aviso!</strong> 
	  <?php
	  foreach ($messages as $message){
		  echo"<p>$message</p>";
	  }
	  ?>
	</div>
	<?php
}
}
?>