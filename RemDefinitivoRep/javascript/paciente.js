
function busquedaGeneral(concepto) {
    var solicitado = document.getElementById(concepto).value;
    var tabla = document.getElementById('tabla').innerHTML;

    switch (tabla) {
        case "Pacientes":
            nuevaPaginacion(tabla, solicitado, 1);
            break;

        case "Personal":
            nuevaPaginacion(tabla, solicitado, 1);
            break;

        case "Instrumental":
            nuevaPaginacion(tabla, solicitado, 1);
            break;

        case "Usuarios":
            nuevaPaginacion(tabla, solicitado, 1);
            break;

        case "QRCode":
            nuevaPaginacion(tabla, solicitado, 1);
            break;

        case "Citas":
            nuevaPaginacion(tabla, solicitado, 1);
            break;

        case "Historias Clínicas":
            nuevaPaginacion("historia_clinica", solicitado, 1);
            break;
    }

}

/***
 * Función para hacer llamado a una paginación.
 * 
 * @param {type} url URL del controlador
 * @param {type} metodo El método de llamado
 * @param {type} index Número de página
 * @param {type} solicitado Parámetro de búsqueda
 * @param {type} tabla Nombre de la tabla donde buscar
 * @param {type} lugarDeRespuesta Donde se mostrará la respuesta del controlador
 * @returns {undefined}
 */
function LlamarControladorPaginacion(url, metodo, index, solicitado, tabla, lugarDeRespuesta){
    $.ajax({
                    type: metodo,
                    url: url,
                    data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
                    success: function (data) {
                        $(lugarDeRespuesta).html(data);
                        //$('#paginas').html(array[1]);
                    }
                });
}

function nuevaPaginacion(tabla, solicitado, index) {

    switch (tabla) {
        case "Pacientes":
            if (solicitado != "") {

                var url = '../controladores/paginarPacientesPor.php';
//                $.ajax({
//                    type: 'POST',
//                    url: url,
//                    data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
//                    success: function (data) {
//                        $('#workArea').html(data);
//                        //$('#paginas').html(array[1]);
//                    }
//                });

                LlamarControladorPaginacion(url, "POST", index, solicitado, tabla, "#workArea");

            } else {
                paginacionPacientes(1);
            }
            break;

        case "Personal":
            var url = '../controladores/paginarMedicosPor.php';
            $.ajax({
                type: 'POST',
                url: url,
                data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
                success: function (data) {
                    $('#workArea').html(data);
                    //$('#paginas').html(array[1]);
                }
            });
            break;

        case "Instrumental":
            var url = '../controladores/paginarInstrumentalPor.php';
            $.ajax({
                type: 'POST',
                url: url,
                data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
                success: function (data) {
                    $('#workArea').html(data);
                    //$('#paginas').html(array[1]);
                }
            });
            break;

        case "Usuarios":
            var url = '../controladores/paginarUsuariosPor.php';
            $.ajax({
                type: 'POST',
                url: url,
                data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
                success: function (data) {
                    $('#workArea').html(data);
                    //$('#paginas').html(array[1]);
                }
            });
            break;

        case "QRCode":
            var url = '../controladores/paginarQRCodigosPor.php';
            $.ajax({
                type: 'POST',
                url: url,
                data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
                success: function (data) {
                    $('#workArea').html(data);
                    //$('#paginas').html(array[1]);
                }
            });
            break;

        case "Citas":

            var url = '../controladores/paginarCitasPor.php';
            $.ajax({
                type: 'POST',
                url: url,
                data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
                success: function (data) {
                    $('#workArea').html(data);
                    //$('#paginas').html(array[1]);
                }
            });
            break;

        case "historia_clinica":

            var url = '../controladores/paginarHistoriasPor.php';
            $.ajax({
                type: 'POST',
                url: url,
                data: 'index=' + index + '&solicitado=' + solicitado + '&tabla=' + tabla,
                success: function (data) {
                    $('#workArea').html(data);
                    //$('#paginas').html(array[1]);
                }
            });
            break;

    }

}

function permitir(control) {
    var tipo = document.getElementById("tipo").innerHTML;

    if (tipo != "Administrador" && tipo != "Médico") {
        document.getElementById(control).setAttribute("disabled", "true");
    }

}

function formNuevoPaciente() {

    var form = '\n\
                </fieldset><h3 class="text-center text-uppercase">Nuevo Paciente</h3>\n\
                <center><form method="POST" class="form-inline" id="formNuevo">\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos de Registro</legend>\n\
                        <label class="etiqueta">Folio/ID:</label>\n\
                        <input onkeyPress="return soloNumeros(event);" style="width: 100px;" id="txtID" type="text" class="form-control" placeholder="ID Paciente">\n\
                        </fieldset>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos Personales</legend>\n\
                        <label class="etiqueta">Nombre:</label>\n\
                        <input onkeypress="return soloLetras(event);" id="txtNombre" type="text" class="form-control" placeholder="Nombre">\n\
                        <label class="etiqueta">Ap. Paterno:</label> <input onkeypress="return soloLetras(event);" id="txtPaterno" type="text" class="form-control" placeholder="Apellido Paterno">\n\
                        <label class="etiqueta">Ap. Materno:</label> <input onkeypress="return soloLetras(event);" id="txtMaterno" type="text" class="form-control" placeholder="Apellido Materno">\n\
                        <label class="etiqueta">Nacimiento:</label> <input readonly onkeypress="return soloFechas(event);" id="txtFechaNac" type="text" class="form-control" placeholder="DD/MM/AAAA" >\n\
                        </fieldset>\n\
                        <select id="cbxSexo" class="btn form-horizontal form-control cajaBusqueda">\n\
                            <option class="alert-danger" selected value=""> Seleccionar Sexo </option>\n\
                            <option class="alert-success" value="Hombre"> Masculino </option>\n\
                            <option class="alert-success" value="Mujer"> Femenino </option>\n\
                            <option class="alert-success" value="Otro"> Otro </option>\n\
                        </select>\n\
                        <select id="cbxEdoCivil" class="btn form-horizontal form-control">\n\
                            <option class="alert-danger " selected value=""> Seleccionar Estado Civil </option>\n\
                            <option class="alert-success " value="Casado"> Casada(o) </option>\n\
                            <option class="alert-success " value="Viudo"> Viuda(o) </option>\n\
                            <option class="alert-success " value="Divorciado"> Divorciada(o) </option>\n\
                            <option class="alert-success " value="Soltero"> Soltera(o) </option>\n\
                            <option class="alert-success " value="Unión Libre"> Unión Libre </option>\n\
                        </select>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos Domiciliarios</legend>\n\
                        <label class="etiqueta">Dirección:</label> <input onkeypress="return soloDirecciones(event);" id="txtDomicilio" type="text" class="form-control" placeholder="Domicilio">\n\
                        <label class="etiqueta">Municipio:</label> <input onkeypress="return soloLetras(event);" id="txtMunicipio" type="text" class="form-control" placeholder="Municipio">\n\
                        <label class="etiqueta">Cod. Postal:</label> <input onkeypress="return soloNumeros(event);" id="txtCP" type="text" class="form-control" placeholder="Codigo Postal">\n\
                        <label class="etiqueta">Ent. Federativa:</label> <input onkeypress="return soloLetras(event);" id="txtEntidadFederativa" type="text" class="form-control" placeholder="Entidad Federativa">\n\
                        <label class="etiqueta">Nacionalidad:</label> <input onkeypress="return soloLetras(event);" id="txtNacionalidad" type="text" class="form-control" placeholder="Nacionalidad">\n\
                        </fieldset>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos de contacto</legend>\n\
                        <label class="etiqueta">Celular:</label> <input maxlength="10" onkeypress="return soloNumeros(event);" id="txtTelCelular" type="text" class="form-control" placeholder="Tel Celular">\n\
                        <label class="etiqueta">Tel. Fijo:</label> <input maxlength="8" onkeypress="return soloNumeros(event);" id="txtTelFijo" type="text" class="form-control" placeholder="Tel Fijo">\n\
                        <label class="etiqueta">E-Mail:</label> <input onkeypress="return soloMails(event);" id="txtMail" type="email" class="text-lowercase form-control" placeholder="Mail">\n\
                        <select id="cbxIdClinica" class="btn form-horizontal form-control"> \n\
                        </select>\n\
                        </fieldset>\n\
                    </form></center>\n\
                    <center><fieldset>\n\
                        <legend id="confirmaciones"></legend>\n\
                        <input onclick="validarRegistroPaciente();" id="nuevoReg" type="button" class="btn btn-success" value="Terminado">\n\
                        <input onclick="paginacionPacientes(1);" type="button" class="btn btn-success" value="Cancelar">\n\
                    </fieldset></center>\n\
                    <script>\n\
                    mostrarCalendario();\n\
                    mostrarClinicas();\n\
                    </script>\n\
                </fieldset>'; //Formulario en cadena para pegar en la vista

    $("#workArea").html(form);
    $("#paginas").html("");
}

/*validaciones pacientes*/

function validarControles(control) {

    var ingresado = document.getElementById(control).value;

    if (control == "txtMail") {
        var ingresado = document.getElementById(control).value;

        if (ingresado.indexOf("@") != -1) {
            return true;
        } else {
            return false;
        }
    } else {
        if (ingresado.trim() != "") {
            return true;
        } else {
            return false;
        }
    }

}

function validarRegistroPaciente() {

    var nombre = document.getElementById("txtNombre");
    var paterno = document.getElementById("txtPaterno");
    var materno = document.getElementById("txtMaterno");
    var fechanac = document.getElementById("txtFechaNac");
    var sexo = document.getElementById("cbxSexo");
    var edocivil = document.getElementById("cbxEdoCivil");
    var domicilio = document.getElementById("txtDomicilio");
    var municipio = document.getElementById("txtMunicipio");
    var cp = document.getElementById("txtCP");
    var entfed = document.getElementById("txtEntidadFederativa");
    var nacionalidad = document.getElementById("txtNacionalidad");
    var celular = document.getElementById("txtTelCelular");
    var telfijo = document.getElementById("txtTelFijo");
    var mail = document.getElementById("txtMail");
    var idclinica = document.getElementById("cbxIdClinica");

    var array = new Array();

    array[0] = nombre;
    array[1] = paterno;
    array[2] = materno;
    array[3] = fechanac;
    array[4] = sexo;
    array[5] = edocivil;
    array[6] = domicilio;
    array[7] = municipio;
    array[8] = cp;
    array[9] = entfed;
    array[10] = nacionalidad;
    array[11] = celular;
    array[12] = telfijo;
    array[13] = mail;
    array[14] = idclinica;

    for (var i = 0; i <= 14; i++) {

        if (i != 11) {

            if (array[i].value.trim().length == 0) {
                $("#" + array[i].id).css("background", "#f0ad4e");
            } else {
                $("#" + array[i].id).css("background", "white");
            }

        }

    }

    if ((array[0].style.background).split(" ")[0] == "white" && (array[1].style.background).split(" ")[0] == "white" &&
            (array[2].style.background).split(" ")[0] == "white" && (array[3].style.background).split(" ")[0] == "white" &&
            (array[4].style.background).split(" ")[0] == "white" && (array[5].style.background).split(" ")[0] == "white" &&
            (array[6].style.background).split(" ")[0] == "white" && (array[7].style.background).split(" ")[0] == "white" &&
            (array[8].style.background).split(" ")[0] == "white" && (array[9].style.background).split(" ")[0] == "white" &&
            (array[10].style.background).split(" ")[0] == "white" && (array[12].style.background).split(" ")[0] == "white" &&
            (array[13].style.background).split(" ")[0] == "white" && (array[14].style.background).split(" ")[0] == "white") {

        var url = '../controladores/pacientesCRUD.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'accion=registrar' + '&nombre=' + array[0].value + '&apPaterno=' + array[1].value + '&apMaterno=' + array[2].value + '&fechaNac=' + array[3].value + '&sexo=' + array[4].value + '&edoCivil=' + array[5].value + '&domicilio=' + array[6].value + '&municipio=' + array[7].value + '&cp=' + array[8].value + '&entidadFederativa=' + array[9].value + '&nacionalidad=' + array[10].value + '&tel_celular=' + array[11].value + '&tel_fijo=' + array[12].value + '&mail=' + array[13].value + '&idClinica=' + array[14].value,
            success: function (data) {
                $('#confirmaciones').html(data);
                window.setTimeout("paginacionPacientes(1);", 2000);
                //$('#paginas').html(array[1]);
            }
        });

    } else {
        $('#confirmaciones').html("Verifique los campos marcados de color.");
    }

}

function eliminarPaciente(id) {
    var url = '../controladores/pacientesCRUD.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'accion=eliminar' + '&idPaciente=' + id,
        success: function (data) {
            if (data == "Eliminación exitosa.") {
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-success"><h1>' + data + '</h1></div>');
                window.setTimeout("paginacionPacientes(1)", 2000);
            } else {
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-danger"><h1 class="alert-danger">El registro no puede ser eliminado.</h1></div>');
                window.setTimeout("paginacionPacientes(1)", 2000);
            }
        }
    });
}

function consultarPaciente(id) {

    var url = '../controladores/pacientesCRUD.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'accion=consultar' + '&idPaciente=' + id,
        success: function (data) {
            var array = data.split('|');
            $('#workArea').html(array[0]);
            $("#paginas").html("");
            seleccionarClinica(array[1]);
        }
    });
}

function seleccionarClinica(valor) {
    window.setTimeout("document.getElementById('cbxIdClinica').value = '" + valor + "'.toString();", 1000);

}


function modificarPrueba() {

    var id = document.getElementById('txtID').value;

    var nombre = document.getElementById("txtNombre");
    var paterno = document.getElementById("txtPaterno");
    var materno = document.getElementById("txtMaterno");
    var fechanac = document.getElementById("txtFechaNac");
    var sexo = document.getElementById("cbxSexo");
    var edocivil = document.getElementById("cbxEdoCivil");
    var domicilio = document.getElementById("txtDomicilio");
    var municipio = document.getElementById("txtMunicipio");
    var cp = document.getElementById("txtCP");
    var entfed = document.getElementById("txtEntidadFederativa");
    var nacionalidad = document.getElementById("txtNacionalidad");
    var celular = document.getElementById("txtTelCelular");
    var telfijo = document.getElementById("txtTelFijo");
    var mail = document.getElementById("txtMail");
    var idclinica = document.getElementById("cbxIdClinica");

    var array = new Array();

    array[0] = nombre;
    array[1] = paterno;
    array[2] = materno;
    array[3] = fechanac;
    array[4] = sexo;
    array[5] = edocivil;
    array[6] = domicilio;
    array[7] = municipio;
    array[8] = cp;
    array[9] = entfed;
    array[10] = nacionalidad;
    array[11] = celular;
    array[12] = telfijo;
    array[13] = mail;
    array[14] = idclinica;

    for (var i = 0; i <= 14; i++) {

        if (i != 11) {

            if (array[i].value.trim().length == 0) {
                $("#" + array[i].id).css("background", "#f0ad4e");
            } else {
                $("#" + array[i].id).css("background", "white");
            }

        }

    }

    if ((array[0].style.background).split(" ")[0] == "white" && (array[1].style.background).split(" ")[0] == "white" &&
            (array[2].style.background).split(" ")[0] == "white" && (array[3].style.background).split(" ")[0] == "white" &&
            (array[4].style.background).split(" ")[0] == "white" && (array[5].style.background).split(" ")[0] == "white" &&
            (array[6].style.background).split(" ")[0] == "white" && (array[7].style.background).split(" ")[0] == "white" &&
            (array[8].style.background).split(" ")[0] == "white" && (array[9].style.background).split(" ")[0] == "white" &&
            (array[10].style.background).split(" ")[0] == "white" && (array[12].style.background).split(" ")[0] == "white" &&
            (array[13].style.background).split(" ")[0] == "white" && (array[14].style.background).split(" ")[0] == "white") {

        var url = '../controladores/pacientesCRUD.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'accion=modificar' + '&idPaciente=' + id + '&nombre=' + array[0].value + '&apPaterno=' + array[1].value + '&apMaterno=' + array[2].value + '&fechaNac=' + array[3].value + '&sexo=' + array[4].value + '&edoCivil=' + array[5].value + '&domicilio=' + array[6].value + '&municipio=' + array[7].value + '&cp=' + array[8].value + '&entidadFederativa=' + array[9].value + '&nacionalidad=' + array[10].value + '&tel_celular=' + array[11].value + '&tel_fijo=' + array[12].value + '&mail=' + array[13].value + '&idClinica=' + array[14].value,
            success: function (data) {
                if (data == 'Modificación exitosa.') {
                    $('#confirmaciones').html(data);
                    window.setTimeout("paginacionPacientes(1);", 3000);
                } else {
                    $('#confirmaciones').html(data);
                }
                //$('#paginas').html(array[1]);
            }
        });

    } else {
        $('#confirmaciones').html("Verifique los campos marcados de color.");
    }
}