function subirHistoria() {//Funcion encargada de enviar el archivo via AJAX
    $(".upload-msg").text('Cargando...');
    
    var inputFileImage = document.getElementById("fileToUpload");
    var file = inputFileImage.files[0];
    mensajeEstilizado("Seleccionó: " + file);
    var data = new FormData();
    data.append('fileToUpload', file);
    data.append('idPaciente', document.getElementById("cbxIdPaciente").value);
    data.append('comentarios',document.getElementById("txtComentarios").value);
    
    $.ajax({
        url: "../controladores/subirHistoria.php", // Url to which the request is send
        type: "POST", // Type of request to be send, called as method
        data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false, // To send DOMDocument or non processed data file it is set to false
        success: function (data)   // A function to be called if request succeeds
        {
            $(".upload-msg").html(data);
            window.setTimeout(function () {
                $(".alert-dismissible").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 5000);
        }
    });

}

function getPSHC(control) {

    var url = '../controladores/PSHC.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: '',
        success: function (data) {
            $("#" + control).html(data);
        }
    });
}

function validarSubida(){
    if(document.getElementById("cbxIdPaciente").value!=""){
        subirHistoria();
        document.getElementById("fileToUpload").value="";
        getPSHC("cbxIdPaciente");
    }else{
        document.getElementById("mensaje").setAttribute("class","upload-msg alert-danger");
        $("#mensaje").html("<center><b>Error!</b><br>Debe seleccionar un paciente al cual asignar la Historia. Comentarios opcionales.</center>");
        window.setTimeout("$('#mensaje').html('')",4000);
    }
}

function formNuevaHistoria(){
    var form = '<div class="container">\n\
                <div class="text-center">\n\
                    <form>\n\
                        <div class="form-group form-inline">\n\
                            <label for="exampleInputFile">Subir historia clínica</label><br><br>\n\
                            <select style="width: 100%;" id="cbxIdPaciente" class="form-control"></select>\n\
                            <textarea id="txtComentarios" style="width: 100%; height: 100px;" class="form-control" placeholder="Comentarios"></textarea>\n\
                            <center><input class="btn btn-default form-horizontal" onchange="alert(this.value);" type="file"  id="fileToUpload"><br><br></center>\n\
                            <input class="btn btn-success" type="button" value="Subir" onclick="validarSubida();">\n\
                            <input class="btn btn-success" type="button" value="Cancelar" onclick="paginacionHistorias();">\n\
                        </div>\n\
                        <div id="mensaje" class="upload-msg"></div><!--Para mostrar la respuesta del archivo llamado via ajax -->\n\
                    </form>\n\
                </div>\n\
                <script>\n\
                    getPSHC("cbxIdPaciente");\n\
                </script>\n\
            </div>';
    
    $("#workArea").html(form);
    $("#paginas").html("");
    
}

function eliminarHistoria(id) {
        var url = '../controladores/historiasCRUD.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'accion=eliminar' + '&idHistoria=' + id,
            success: function (data) {
                if(data=="Eliminación exitosa."){
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-success"><h1>' + data + '</h1></div>');
                window.setTimeout("paginacionHistorias(1)",2000);
            }else{
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-danger"><h1 class="alert-danger">El registro no puede ser eliminado.</h1></div>');
                window.setTimeout("paginacionHistorias(1)",2000);
            }
            }
        });
    }