
function nuevoCertificado(){
    var form = '<center><form class="form-inline" method="post">\n\
            <fieldset>\n\
                <legend class="text-uppercase text-info" >Nuevo Certificado Medico</legend>\n\
                <fieldset>\n\
                <legend class="text-uppercase alert-info text-info">Personales Y Fecha</legend>\n\
                <label>Nombre: </label>\n\
                <input onkeypress="return soloLetras(event);" class="form-control" placeholder="Nombre" type="text" name="txtNombre" id="txtNombre">\n\
                <label>Sexo: </label>\n\
                <select class="btn form-control form-horizontal cajaBusqueda" name="cbxSexo" id="cbxSexo">\n\
                    <option class="alert-danger" value="">Seleccionar Sexo: </option>\n\
                    <option class="alert-success" value="Masculino">Masculino</option>\n\
                    <option class="alert-success" value="Femenino">Femenino</option>\n\
                </select>\n\
                <label>Fecha: </label>\n\
                <input readonly onkeypress="return soloFechas(event);" name="txtFecha" id="txtFechaNac" type="text" class="form-control" placeholder="AAAA/MM/DD" >\n\
                </fieldset>\n\
                <fieldset>\n\
                <legend class="text-uppercase alert-info text-info">Enfermedades General:</legend>\n\
                <label>¿Orgánica/Infecciosa/Transmisible?: </label>\n\
                <select class="btn form-control form-horizontal cajaBusqueda" name="cbxOrganica" id="cbxOrganica">\n\
                    <option class="alert-danger" value="">Seleccionar: </option>\n\
                    <option class="alert-success" value="Sí">Sí</option>\n\
                    <option class="alert-success" value="No">No</option>\n\
                </select>\n\
                <label>Crónica: </label>\n\
                <select class="btn form-control form-horizontal cajaBusqueda" name="cbxCronica" id="cbxCronica">\n\
                    <option class="alert-danger" value="">Seleccionar: </option>\n\
                    <option class="alert-success" value="Sí">Sí</option>\n\
                    <option class="alert-success" value="No">No</option>\n\
                </select>\n\
                </fieldset>\n\
                <fieldset>\n\
                <legend class="text-uppercase alert-info text-info">Comentarios: </legend>\n\
                <textarea placeholder="Los comentarios son opcionales" onkeypress="return alfaNumericos(event);" class="form-control" style="width:100%; heigth:100px;" name="txtComentarios" id="txtComentarios"></textarea>\n\
                </fieldset>\n\
                <fieldset>\n\
                <legend class="text-uppercase alert-info text-info">Información Paciente</legend>\n\
                <label>Grupo Sanguíneo: </label>\n\
                <select class="btn form-control form-horizontal cajaBusqueda" name="cbxGSanguineo" id="cbxGSanguineo">\n\
                    <option class="alert-danger" value="">Seleccionar: </option>\n\
                    <option class="alert-success" value="AB+">AB+</option>\n\
                    <option class="alert-success" value="AB-">AB-</option>\n\
                    <option class="alert-success" value="A+">A+</option>\n\
                    <option class="alert-success" value="A-">A-</option>\n\
                    <option class="alert-success" value="B+">B+</option>\n\
                    <option class="alert-success" value="B-">B-</option>\n\
                    <option class="alert-success" value="O+">O+</option>\n\
                    <option class="alert-success" value="O-">O-</option>\n\
                </select>\n\
                <label>Factor Rh: </label>\n\
                <select class="btn form-control form-horizontal cajaBusqueda" name="cbxRH" id="cbxRH">\n\
                    <option class="alert-danger" value="">Seleccionar: </option>\n\
                    <option class="alert-success" value="Positivo (+)">Positivo (+)</option>\n\
                    <option class="alert-success" value="Negativo (-)">Negativo (-)</option>\n\
                </select>\n\
                </fieldset>\n\
                <fieldset>\n\
                <legend id="confirmaciones"></legend>\n\
                <input class="btn btn-success" onclick="validarRegistroCertificado();" value="Terminado">\n\
                <input class="btn btn-warning" value="Cancelar">\n\
                </fieldset>\n\
            </fieldset>\n\
        </form></center>\n\
<script>mostrarCalendario();</script>';
    
    $("#workArea").html(form);
    $("#paginas").html("");
        
}

function validarRegistroCertificado(){
    var nombre = document.getElementById("txtNombre");
    var sexo = document.getElementById("cbxSexo");
    var fecha = document.getElementById("txtFechaNac");
    var oit = document.getElementById("cbxOrganica");
    var cronica = document.getElementById("cbxCronica");
    var comentarios = document.getElementById("txtComentarios");
    var gsanguineo = document.getElementById("cbxGSanguineo");
    var rh = document.getElementById("cbxRH");
    
    var continuar = false;
    
    var array = new Array();
    
    array[0] = nombre;
    array[1] = sexo;
    array[2] = fecha;
    array[3] = oit;
    array[4] = cronica;
    array[5] = comentarios;
    array[6] = gsanguineo;
    array[7] = rh;
    
    for (var i = 0; i <= 7; i++){
        
        if(i!=5){
        
        if(array[i].value.trim().length == 0){
            $("#"+array[i].id).css("background", "#f0ad4e");
        }else{
            $("#"+array[i].id).css("background", "white");
        }
    
        }
        
    }
    
    if((array[0].style.background).split(" ")[0]=="white" && (array[1].style.background).split(" ")[0]=="white" &&
            (array[2].style.background).split(" ")[0]=="white" &&(array[3].style.background).split(" ")[0]=="white" &&
            (array[4].style.background).split(" ")[0]=="white" && (array[6].style.background).split(" ")[0]=="white" 
            &&(array[7].style.background).split(" ")[0]=="white"){
        
        var url = '../controladores/generarCertificado.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=registrar' + '&nombre=' + array[0].value + '&sexo=' + array[1].value + '&fechaNac=' + array[2].value + '&organica=' + array[3].value + '&cronica=' + array[4].value + '&comentarios=' + array[5].value + '&gSanguineo=' + array[6].value + '&rH=' + array[7].value,
                success: function (data) {
                    $('#confirmaciones').html("Completado");
                    window.location = data;
                }
        });
        
    }else{
        $('#confirmaciones').html("Verifique los campos marcados de color.");
    }
    
}

function utf8_decode (strData) { // eslint-disable-line camelcase
  //  discuss at: http://locutus.io/php/utf8_decode/
  // original by: Webtoolkit.info (http://www.webtoolkit.info/)
  //    input by: Aman Gupta
  //    input by: Brett Zamir (http://brett-zamir.me)
  // improved by: Kevin van Zonneveld (http://kvz.io)
  // improved by: Norman "zEh" Fuchs
  // bugfixed by: hitwork
  // bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
  // bugfixed by: Kevin van Zonneveld (http://kvz.io)
  // bugfixed by: kirilloid
  // bugfixed by: w35l3y (http://www.wesley.eti.br)
  //   example 1: utf8_decode('Kevin van Zonneveld')
  //   returns 1: 'Kevin van Zonneveld'
  var tmpArr = []
  var i = 0
  var c1 = 0
  var seqlen = 0
  strData += ''
  while (i < strData.length) {
    c1 = strData.charCodeAt(i) & 0xFF
    seqlen = 0
    // http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
    if (c1 <= 0xBF) {
      c1 = (c1 & 0x7F)
      seqlen = 1
    } else if (c1 <= 0xDF) {
      c1 = (c1 & 0x1F)
      seqlen = 2
    } else if (c1 <= 0xEF) {
      c1 = (c1 & 0x0F)
      seqlen = 3
    } else {
      c1 = (c1 & 0x07)
      seqlen = 4
    }
    for (var ai = 1; ai < seqlen; ++ai) {
      c1 = ((c1 << 0x06) | (strData.charCodeAt(ai + i) & 0x3F))
    }
    if (seqlen === 4) {
      c1 -= 0x10000
      tmpArr.push(String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF)))
      tmpArr.push(String.fromCharCode(0xDC00 | (c1 & 0x3FF)))
    } else {
      tmpArr.push(String.fromCharCode(c1))
    }
    i += seqlen
  }
  return tmpArr.join('')
}