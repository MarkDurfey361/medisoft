function mostrarCalendarioCitas(){
    
    $("#txtFechaCita").datepicker(
            {
                changeMonth: true,
                changeYear:true,
                showButtonPanel:true,
                dateFormat: "yy-mm-dd",
                minDate: "+1D",
                maxDate: "+1Y"
            }
    );
}

function mostrarPacientes(){
    $.ajax({
        type: 'POST',
        url: '../controladores/pacientesCita.php',
        data: '',
        success: function (confirmacion) {
            $('#cbxIdPaciente').html(confirmacion);
        }
    });
};

function validarRegistroCita(accion){
    validarHora("cbxHoras","cbxMinutos","txtFechaCita","cbxIdPaciente",accion);
}

function validarHora(horas,minutos,fecha,idPaciente,accion){
    
    var hora =document.getElementById(horas).value;
    var minuto = document.getElementById(minutos).value;
    var paciente = document.getElementById(idPaciente).value;
    var dia = document.getElementById(fecha).value;
    
    var horaSol = hora+":"+minuto+":00";
    
    if(hora!="" && minuto!="" && paciente!="" && dia!=""){
        if(accion=="registrar"){
            registrarCita(paciente, dia,horaSol);
        }else{
            modificarCita(paciente, dia,horaSol);
        }
    }else{
        $("#confirmaciones").html("Debe seleccionarse tanto un paciente como la fecha y hora de la cita.");
    }
}

function registrarCita(id,fecha,hora){
    $.ajax({
        type: 'POST',
        url: '../controladores/citasCRUD.php',
        data: 'accion=registrar' + '&idPaciente=' + id + '&fecha=' + fecha +'&hora=' + hora,
        success: function (confirmacion) {
            $('#confirmaciones').html(confirmacion);
            if(confirmacion=="Registro exitoso."){
            window.setTimeout("paginacionCitas(1);",2000);
        }
        }
    });
}

function modificarCita(id,fecha,hora){
    $.ajax({
        type: 'POST',
        url: '../controladores/citasCRUD.php',
        data: 'accion=modificar' + '&idPaciente=' + id + '&fecha=' + fecha +'&hora=' + hora + '&idCita=' + document.getElementById("txtID").value,
        success: function (confirmacion) {
            $('#confirmaciones').html(confirmacion);
            if(confirmacion=="Modificación exitosa."){
            window.setTimeout("paginacionCitas(1);",2000);
            }
        }
    });
}

function formNuevaCita(){
    var form = '<center>\n\
          <form class="form-inline" method="POST">\n\
                    <fieldset>\n\
                        <legend class="text-uppercase alert-info text-info">Datos de la cita.</legend>\n\
                        <label class="etiqueta">ID/Folio:</label>\n\
                        <input onkeyPress="return soloNumeros(event);" style="width: 100px;" id="txtID" type="text" class="form-control" placeholder="ID Cita">\n\
                        <select id="cbxIdPaciente" class="btn form-control form-horizontal">\n\
                        </select>\n\
                        <label class="etiqueta">Fecha:</label>\n\
                        <input readonly onkeypress="return soloFechas(event);" id="txtFechaCita" type="text" class="form-control" placeholder="DD/MM/AAAA" >\n\
                        <br><label class="etiqueta">Horas/Minutos:</label><br><select id="cbxHoras" class="btn form-horizontal form-control">\n\
                            <option class="alert-danger" value="">Horas</option>\n\
                            <option class="alert-success" value="01">01</option>\n\
                            <option class="alert-success" value="02">02</option>\n\
                            <option class="alert-success" value="03">03</option>\n\
                            <option class="alert-success" value="04">04</option>\n\
                            <option class="alert-success" value="05">05</option>\n\
                            <option class="alert-success" value="06">06</option>\n\
                            <option class="alert-success" value="07">07</option>\n\
                            <option class="alert-success" value="08">08</option>\n\
                            <option class="alert-success" value="09">09</option>\n\
                            <option class="alert-success" value="10">10</option>\n\
                            <option class="alert-success" value="11">11</option>\n\
                            <option class="alert-success" value="12">12</option>\n\
                            <option class="alert-success" value="13">13</option>\n\
                            <option class="alert-success" value="14">14</option>\n\
                            <option class="alert-success" value="15">15</option>\n\
                            <option class="alert-success" value="16">16</option>\n\
                            <option class="alert-success" value="17">17</option>\n\
                            <option class="alert-success" value="18">18</option>\n\
                            <option class="alert-success" value="19">19</option>\n\
                            <option class="alert-success" value="20">20</option>\n\
                            <option class="alert-success" value="21">21</option>\n\
                            <option class="alert-success" value="22">22</option>\n\
                            <option class="alert-success" value="23">23</option>\n\
                            <option class="alert-success" value="24">24</option>\n\
                        </select>\n\
                        :\n\
                        <select id="cbxMinutos" class="btn form-horizontal form-control">\n\
                            <option class="alert-danger" value="">Minutos</option>\n\
                            <option class="alert-success" value="00">00</option>\n\
                            <option class="alert-success" value="05">05</option>\n\
                            <option class="alert-success" value="10">10</option>\n\
                            <option class="alert-success" value="15">15</option>\n\
                            <option class="alert-success" value="20">20</option>\n\
                            <option class="alert-success" value="25">25</option>\n\
                            <option class="alert-success" value="30">30</option>\n\
                            <option class="alert-success" value="35">35</option>\n\
                            <option class="alert-success" value="40">40</option>\n\
                            <option class="alert-success" value="45">45</option>\n\
                            <option class="alert-success" value="50">50</option>\n\
                            <option class="alert-success" value="55">55</option>\n\
                        </select>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase text-info" id="confirmaciones"></legend>\n\
                        </fieldset>\n\
                        <input id="btnRegistrarCita" onclick="validarRegistroCita("registrar");" type="button" class="btn btn-success" value="Terminado">\n\
                        <input onclick="paginacionCitas(1);" type="button" class="btn btn-success" value="Cancelar">\n\
                    </fieldset>\n\
                </form>\n\
                <script>\n\
                    mostrarCalendarioCitas();\n\
                    mostrarPacientes();\n\
                    </script>\n\
            </center>';
        
    $("#workArea").html(form);
    $("#paginas").html("");       
}

function consultarCita(id){
    var url = '../controladores/citasCRUD.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'accion=consultar' + '&idPaciente=' + id,
        success: function (data) {
            var array = data.split('|');
            $('#workArea').html(array[0]);
            seleccionarPaciente(array[1]);
            $("#paginas").html("");
        }
    });
}

function seleccionarPaciente(valor) {
    window.setTimeout("document.getElementById('cbxIdPaciente').value = '"+valor+"'.toString();",1000);
}

function eliminarCita(id) {
        var url = '../controladores/citasCRUD.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'accion=eliminar' + '&idPaciente=' + id,
            success: function (data) {
                if(data=="Eliminación exitosa."){
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-success"><h1>' + data + '</h1></div>');
                window.setTimeout("paginacionCitas(1)",2000);
            }else{
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-danger"><h1>El registro no puede ser eliminado.</h1></div>');
                window.setTimeout("paginacionCitas(1)",2000);
            }
            }
        });
    }
    
