function mensajeEstilizado(cuerpo){
 var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Close</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3>'+cuerpo+'</h3></center>');
    $("#myModal").modal();
};

function mensajeCerrarSesion(){
 var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Seguir usando</button>\n\
            <button type="button" onclick="cerrarSesion();" class="btn btn-lg alert-danger" data-dismiss="modal">Salir</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">A punto de dejar el sistema.</h3></center>');
    $("#myModal").modal();
};

function mensajeEliminar(idPaciente){
    var tabla = document.getElementById("tabla").innerHTML;
    
    switch (tabla){
        case "Pacientes":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="eliminarPaciente('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Eliminar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Personal":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="eliminarPersonal('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Eliminar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Instrumental":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="eliminarInstrumento('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Eliminar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Usuarios":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="eliminarUsuario('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Eliminar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Citas":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="eliminarCita('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Eliminar?</h3></center>');
    $("#myModal").modal();
        break;
        case "QRCode":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="eliminarQR('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Eliminar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Historias Clínicas":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="eliminarHistoria('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Eliminar?</h3></center>');
    $("#myModal").modal();
        break;
    }
    
}

function mensajeVisualizar(idPaciente){
    var tabla = document.getElementById("tabla").innerHTML;
    
    switch (tabla){
        case "Pacientes":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="consultarPaciente('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Modificar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Personal":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="consultarPersonal('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Modificar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Instrumental":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="consultarInstrumental('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Modificar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Usuarios":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="consultarUsuario('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Modificar?</h3></center>');
    $("#myModal").modal();
        break;
        case "Citas":
            var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Cancelar</button>\n\
            <button type="button" onclick="consultarCita('+idPaciente+');" class="btn btn-lg alert-danger" data-dismiss="modal">Sí</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    $('#moda').html(codigo);
    $('#cuerpo').html('<center><h3 class="text-info">¿Modificar?</h3></center>');
    $("#myModal").modal();
        break;
    }
    
}