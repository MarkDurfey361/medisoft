function cerrarSesion(){
        window.location = '../controladores/cerrarSesion.php';
}

function lanzarCaja(opcion){
    switch (opcion){
        case "ID":
            div = document.getElementById("cajaDeBusqueda");
            $("#cajaDeBusqueda").html("<input id='porId' type='text' class='form-control cajaBusqueda' placeholder='ID o Folio' onkeypress='return soloNumeros(event);' onkeyup='buscarPor(this.id);'>");
            break;
            
        case "Nombre":
            div = document.getElementById("cajaDeBusqueda");
            $("#cajaDeBusqueda").html("<input id='porNombres' type='text' class='form-control cajaBusqueda' placeholder='Nombre(s)' onkeypress='return soloLetras(event);' onkeyup='buscarPor(this.id);'>");
            break;
            
        case "Mail":
            div = document.getElementById("cajaDeBusqueda");
            $("#cajaDeBusqueda").html("<input id='porMail' type='text' class='form-control cajaBusqueda' placeholder='Mail' onkeypress='return soloMails(event);' onkeyup='buscarPor(this.id);'>");
            break;
            
        default:
            div = document.getElementById("cajaDeBusqueda");
            $("#cajaDeBusqueda").html("");
            break;
    }
}

function mostrarEspecialidades() {

    var accion = "especialidades";
    
    $.ajax({
        type: 'POST',
        url: '../controladores/obtenciones.php',
        data: ('accion=' + accion),
        success: function (confirmacion) {
            $('#cbxEspecialidad').html(confirmacion);

        }
    });
};

function mostrarClinicas() {

    var accion = "clinicas";

    $.ajax({
        type: 'POST',
        url: '../controladores/obtenciones.php',
        data: ('accion=' + accion),
        success: function (confirmacion) {
            $('#cbxIdClinica').html(confirmacion);
        }
    });
}

function mostrarCalendario(){
    
    var anoActual = new Date();
    
    $("#txtFechaNac").datepicker(
            {
                changeMonth: true,
                changeYear:true,
                showButtonPanel:true,
                dateFormat: "yy-mm-dd",
                yearRange: "1940:"+(anoActual.getFullYear())
            }
    );
}

function incrustarModal(id,complemento){
 var codigo = '<!-- Modal -->\n\
  <div class="modal fade" id="myModal" role="dialog">\n\
    <div class="modal-dialog">\n\
      <!-- Modal content-->\n\
      <div class="modal-content">\n\
        <div class="modal-header">\n\
          <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
          <center><img src="../resources/logoMedisoft.png" class="img img-responsive" width="40%" height="40%" alt="forcep"></center>\n\
        </div>\n\
          <div id="cuerpo" class="modal-body">\n\
              \n\
        </div>\n\
        <div class="modal-footer">\n\
        <h4 class="text-capitalize text-success">Clic sobre la ficha para imprimir.</h4>\n\
            <button type="button" class="btn btn-lg alert-info" data-dismiss="modal">Close</button>\n\
        </div>\n\
      </div>\n\
    </div>\n\
  </div>\n\
</div>';
    
    var cuerpo = '<div onclick="imprimirQR(this.id);" id="codigoQR"><table>\n\
            <tr>\n\
                <td id="col1"><center><img src="../QR/'+id+'.png" alt="QR'+id+'"></center></td>\n\
        <td id="col2"><center><img style="width: 100%;" src="../resources/logoRemedioss.png"></center></td>\n\
            </tr>\n\
            <tr>\n\
                    <td id="col3"><center>Foto Paciente.<br>No dañar el código.<br>Rec. enmicar.</center></td>\n\
                <td id="col4"><center><h2>'+id+' <br> '+complemento+'</h2></center></td>\n\
            </tr>\n\
        </table>\n\
    </div>';
    
    $('#moda').html(codigo);
    $('#cuerpo').html('<center>'+cuerpo+'</center>');
    $("#myModal").modal();
}

function imprimirQR(nombreDiv) {
    var objeto=document.getElementById(nombreDiv);  //obtenemos el objeto a imprimir
    var ventana=window.open('','_blank');  //abrimos una ventana vacía nueva
    ventana.document.write('<head><title>Imprimir Ficha</title><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><style>body{font-size: 20px;font-family: sans-serif;}table{border-radius: 19px 19px 19px 19px;border: solid 1px black;}#col1{border-style: dotted; border-width: 1px; border-color: 660033; border-radius: 19px 19px 19px 19px;border-bottom: 0px;border-right: 0px;height: 150px;width: 300px;}#col2{border-style: dotted;border-width: 1px; border-color: 660033; border-radius: 19px 19px 19px 19px;border-bottom: 0px;border-left: 0px;height: 150px;width: 300px;}#col3{border-style: dotted; border-width: 1px; border-color: 660033; border-radius: 19px 19px 19px 19px;border-top: 0px;border-right: 0px;height: 150px;width: 300px;}#col4{border-style: dotted; border-width: 1px; border-color: 660033; border-radius: 19px 19px 19px 19px;border-top: 0px;border-left: 0px;height: 150px;width: 300px;}img{width: 60%;}#espacioFoto{width: 80%;border: solid 1px black;}</style></head>');
    ventana.document.write('<body>');
    ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
    ventana.document.write('</body>');
    ventana.document.close();  //cerramos el documento
    ventana.print();  //imprimimos la ventana
    ventana.close();  //cerramos la ventana
 }