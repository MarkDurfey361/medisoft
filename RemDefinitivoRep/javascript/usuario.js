function formNuevoUsuario(){

var form = '</fieldset><h3 class="text-center text-uppercase">Nuevo Usuario</h3>\n\
                <center><form method="POST" class="form-inline" id="formNuevo">\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos de Registro</legend>\n\
                        <label class="etiqueta">ID/Folio:</label><input onkeyPress="return soloNumeros(event);" style="width: 100px;" id="txtID" type="text" class="form-control" placeholder="ID Usuario">\n\
                        <select id="cbxPSU" class="btn form-control form-horizontal"></select>\n\
                        </fieldset>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos del usuario</legend>\n\
                        <label class="etiqueta">Nombre Usuario:</label><input onkeypress="return soloLetras(event);" id="txtNombre" type="text" class="form-control" placeholder="Nombre">\n\
                        <label class="etiqueta">Contraseña:</label><input onkeypress="return soloLetras(event);" id="txtPassword" type="text" class="form-control" placeholder="Password">\n\
                        <select id="cbxTipoUsuario" class="btn form-horizontal form-control">\n\
                            <option class="alert-danger " selected value=""> Seleccionar Tipo: </option>\n\
                            <option class="alert-success " value="Recepcionista"> Recepcionista </option>\n\
                            <option class="alert-success " value="Enfermera"> Enfermera </option>\n\
                            <option class="alert-success " value="Medico"> Medico </option>\n\
                            <option class="alert-success " value="Almacenista"> Almacenista </option>\n\
                            <option class="alert-success " value="Administrador"> Administrador </option>\n\
                        </select>\n\
                        </fieldset>\n\
                        <fieldset>\n\
                        <legend id="confirmaciones"></legend>\n\
                        <input onclick="a();" type="button" class="btn btn-success" value="Terminado">\n\
                        <input onclick="paginacionUsuarios(1);" type="button" class="btn btn-success" value="Cancelar">\n\
                    </fieldset></center>\n\
                <script>\n\
                    getPSU("cbxPSU");\n\
                </script>\n\
                </fieldset>';
        $("#workArea").html(form);
        $("#paginas").html("");
        }

function a(){
var array = [4];
        array[0] = document.getElementById("cbxPSU").id;
        array[1] = document.getElementById("txtNombre").id;
        array[2] = document.getElementById("txtPassword").id;
        array[3] = document.getElementById("cbxTipoUsuario").id;
        if (validarControles(array[0]) == true){
if (validarControles(array[1]) == true){
if (validarControles(array[2]) == true){
if (validarControles(array[3]) == true){
var url = '../controladores/usuariosCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=registrar' + '&idPSU=' + document.getElementById(array[0]).value + '&nombre=' + document.getElementById(array[1]).value + '&password=' + document.getElementById(array[2]).value + '&tipo=' + document.getElementById(array[3]).value,
                success: function (data) {
                if (data == 'Registro exitoso.') {
                $('#confirmaciones').html(data);
                        window.setTimeout("paginacionUsuarios(1);", 3000);
                } else {
                $('#confirmaciones').html(data);
                }
                }
        });
} else{
$('#confirmaciones').html("Imposible Continuar, no seleccionó ningún tipo");
}
} else{
$('#confirmaciones').html("Imposible Continuar, verifique el password ingresada");
}
} else{
$('#confirmaciones').html("Imposible Continuar, verifique el nombre ingresado");
}
} else{
$('#confirmaciones').html("Imposible Continuar, verifique el ID Personal ingresado");
}
}

function getPSU(control){

var url = '../controladores/PSU.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'parametro=sin',
                success: function (data) {
                $("#" + control).html(data);
                }
        });
        }

function getPCU(control){

var url = '../controladores/PSU.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'parametro=con',
                success: function (data) {
                $("#" + control).html(data);
                }
        });
        }

function eliminarUsuario(id){
var url = '../controladores/usuariosCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=eliminar' + '&idUsuario=' + id,
                success: function (data) {
                if(data=="Eliminación exitosa."){
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-success"><h1>' + data + '</h1></div>');
                window.setTimeout("paginacionUsuarios(1)",2000);
            }else{
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-danger"><h1>El registro no puede ser eliminado.</h1></div>');
                window.setTimeout("paginacionUsuarios(1)",2000);
            }
                }
        });
}

function consultarUsuario(id) {

var url = '../controladores/usuariosCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=consultar' + '&idUsuario=' + id,
                success: function (data) {
                var array = data.split('|');
                        $('#workArea').html(array[0]);
                        $("#paginas").html("");
                        seleccionarPersonal(array[1]);
                }
        });
        }

function seleccionarPersonal(valor){
window.setTimeout("document.getElementById('cbxPSU').value='" + valor + "'.toString();", 1000);
        }

function modificarUsuario(){

var id = document.getElementById("txtID").value;
        var array = [3];
        array[0] = document.getElementById("txtNombre").id;
        array[1] = document.getElementById("txtPassword").id;
        array[2] = document.getElementById("cbxTipoUsuario").id;
        if (validarControles(array[0]) == true){
if (validarControles(array[1]) == true){
if (validarControles(array[2]) == true){
var url = '../controladores/usuariosCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=modificar' + '&idUsuario=' + id + '&nombre=' + document.getElementById(array[0]).value + '&password=' + document.getElementById(array[1]).value + '&tipo=' + document.getElementById(array[2]).value,
                success: function (data) {
                if (data == 'Modificación exitosa.') {
                $('#confirmaciones').html(data);
                        window.setTimeout("paginacionUsuarios(1);", 3000);
                } else {
                $('#confirmaciones').html(data);
                }
                }
        });
} else{
$('#confirmaciones').html("Imposible Continuar, no seleccionó ningún tipo");
}
} else{
$('#confirmaciones').html("Imposible Continuar, verifique el password ingresada");
}
} else{
$('#confirmaciones').html("Imposible Continuar, verifique el nombre ingresado");
}
}

/* FOTOS DE PERFIL */

function formFotoPerfil(){
    var form = '<div class="container">\n\
                <div class="text-center">\n\
                    <form>\n\
                        <div class="form-group form-inline">\n\
                            <label for="exampleInputFile">Subir foto de perfil</label><br><br>\n\
                            <input hidden readonly="true" id="txtNombreUsuario" class="form-control" type="text" placeholder="Nombre de usuario">\n\
                            <center><input class="btn btn-default form-horizontal" type="file"  id="fileToUpload"><br></center>\n\
                            <input class="btn btn-default" type="button" value="Subir" onclick="subirFoto();">\n\
                        </div>\n\
                        <div id="mensaje" class="upload-msg"></div><!--Para mostrar la respuesta del archivo llamado via ajax -->\n\
                    </form>\n\
                </div>\n\
                <script>\n\
                    obtenerUsuario();\n\
                </script>\n\
            </div>';
    
    $("#workArea").html(form);
    $("#paginas").html("");
    
}

function obtenerUsuario(){
    document.getElementById('txtNombreUsuario').value = document.getElementById('Conectado').innerHTML;
}

function subirFoto(){
    alert("lsto");
    $(".upload-msg").text('Cargando...');
    
    var inputFileImage = document.getElementById("fileToUpload");
    var file = inputFileImage.files[0];
    var data = new FormData();
    data.append('fileToUpload', file);
    data.append('nombreUsuario', document.getElementById("txtNombreUsuario").value);
   
    $.ajax({
        url: "../controladores/subirFotoPerfil.php", // Url to which the request is send
        type: "POST", // Type of request to be send, called as method
        data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false, // The content type used when sending data to the server.
        cache: false, // To unable request pages to be cached
        processData: false, // To send DOMDocument or non processed data file it is set to false
        success: function (data)   // A function to be called if request succeeds
        {
            $(".upload-msg").html(data);
            window.setTimeout(function () {
                $(".alert-dismissible").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 5000);
        }
    });
    
}