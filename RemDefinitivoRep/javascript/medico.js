
function formNuevoPersonal(){

var form = '</fieldset><h3 class="text-center text-uppercase">Nuevo Personal</h3>\n\
                <center><form method="POST" class="form-inline" id="formNuevo">\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos de Registro</legend>\n\
                        <label class="etiqueta">ID/Folio:</label><input onkeyPress="return soloNumeros(event);" style="width: 100px;" id="txtID" type="text" class="form-control" placeholder="ID Médico">\n\
                        <select id="cbxTipoPersonal" class="btn form-control form-horizontal cajaBusqueda">\n\
<option class="alert-danger" selected value="" > Seleccionar Tipo: </option>\n\
<option class="alert-success" value="Recepcionista" > Recepcionista </option>\n\
<option class="alert-success" value="Enfermera" > Enfermera </option>\n\
<option class="alert-success" value="Medico" > Medico </option>\n\
<option class="alert-success" value="Almacenista" > Almacenista </option>\n\
<option class="alert-success" value="Administrador" > Administrador </option>\n\
</select>\n\
                        </fieldset>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos Personales</legend>\n\
                        <label class="etiqueta">Nombre:</label><input onkeypress="return soloLetras(event);" id="txtNombre" type="text" class="form-control" placeholder="Nombre">\n\
                        <label class="etiqueta">Ap. Paterno:</label><input onkeypress="return soloLetras(event);" id="txtPaterno" type="text" class="form-control" placeholder="Apellido Paterno">\n\
                        <label class="etiqueta">Ap. Materno:</label><input onkeypress="return soloLetras(event);" id="txtMaterno" type="text" class="form-control" placeholder="Apellido Materno">\n\
                        <label class="etiqueta">Nacimiento:</label><input readonly onkeypress="return soloFechas(event);" id="txtFechaNac" type="text" class="form-control" placeholder="DD/MM/AAAA" >\n\
                        </fieldset>\n\
                        <select id="cbxSexo" class="btn form-horizontal form-control cajaBusqueda">\n\
                            <option class="alert-danger" selected value=""> Seleccionar Sexo </option>\n\
                            <option class="alert-success" value="Hombre"> Masculino </option>\n\
                            <option class="alert-success" value="Mujer"> Femenino </option>\n\
                            <option class="alert-success" value="Otro"> Otro </option>\n\
                        </select>\n\
                        <select id="cbxEdoCivil" class="btn form-horizontal form-control">\n\
                            <option class="alert-danger " selected value=""> Seleccionar Estado Civil </option>\n\
                            <option class="alert-success " value="Casado"> Casada(o) </option>\n\
                            <option class="alert-success " value="Viudo"> Viuda(o) </option>\n\
                            <option class="alert-success " value="Divorciado"> Divorciada(o) </option>\n\
                            <option class="alert-success " value="Soltero"> Soltera(o) </option>\n\
                            <option class="alert-success " value="Unión Libre"> Unión Libre </option>\n\
                        </select>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos Domiciliarios</legend>\n\
                        <label class="etiqueta">Domicilio:</label><input onkeypress="return soloDirecciones(event);" id="txtDomicilio" type="text" class="form-control" placeholder="Domicilio">\n\
                        <label class="etiqueta">Municipio:</label><input onkeypress="return soloLetras(event);" id="txtMunicipio" type="text" class="form-control" placeholder="Municipio">\n\
                        <label class="etiqueta">Cod. Postal:</label><input onkeypress="return soloNumeros(event);" id="txtCP" type="text" class="form-control" placeholder="Codigo Postal">\n\
                        <label class="etiqueta">Ent. Federativa:</label><input onkeypress="return soloLetras(event);" id="txtEntidadFederativa" type="text" class="form-control" placeholder="Entidad Federativa">\n\
                        <label class="etiqueta">Nacionalidad:</label><input onkeypress="return soloLetras(event);" id="txtNacionalidad" type="text" class="form-control" placeholder="Nacionalidad">\n\
                        </fieldset>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos de contacto</legend>\n\
                        <label class="etiqueta">Celular:</label><input maxlength="10" onkeypress="return soloNumeros(event);" id="txtTelCelular" type="text" class="form-control" placeholder="Tel Celular">\n\
                        <label class="etiqueta">Tel. Fijo:</label><input maxlength="8" onkeypress="return soloNumeros(event);" id="txtTelFijo" type="text" class="form-control" placeholder="Tel Fijo">\n\
                        <label class="etiqueta">E-Mail:</label><input onkeypress="return soloMails(event);" id="txtMail" type="email" class="text-lowercase form-control" placeholder="Mail">\n\
                        <select id="cbxIdClinica" class="btn form-horizontal form-control"> \n\
                        </select>\n\
                        </fieldset>\n\
                        <fieldset>\n\
<legend class="text-uppercase alert-info text-info">Datos Profesionales</legend>\n\
<label class="etiqueta">RFC:</label><input maxlength="13" onkeypress="return alfaNumericos(event);" id="txtRFC" type="text" class="form-control" placeholder="RFC">\n\
<label class="etiqueta">Título:</label><input onkeypress="return soloLetras(event);" id="txtTitulo" type="text" class="form-control" placeholder="Título">\n\
<select id="cbxEspecialidad" class="btn form-horizontal form-control"></select>\n\
<label class="etiqueta">Cédula:</label><input maxlength="8" onkeypress="return soloNumeros(event);" id="txtCedula" type="text" class="form-control" placeholder="Cédula Profesional">\n\
</fieldset>\n\
                    </form></center>\n\
                    <center><fieldset>\n\
                        <legend id="confirmaciones"></legend>\n\
                        <input id="btnRegistrarMedico" onclick="validarRegistroMedicoN();" type="button" class="btn btn-success" value="Terminado">\n\
                        <input onclick="paginacionMedicos(1);" type="button" class="btn btn-success" value="Cancelar">\n\
                    </fieldset></center>\n\
                    <script>\n\
                    mostrarCalendario();\n\
                    mostrarClinicas();\n\
                    mostrarEspecialidades();\n\
                    </script>\n\
                </fieldset>';
        $("#workArea").html(form);
        $("#paginas").html("");
        
        }

function validarRegistroMedicoN() {

        var tipo = document.getElementById("cbxTipoPersonal");
        var nombre = document.getElementById("txtNombre");
        var paterno = document.getElementById("txtPaterno");
        var materno = document.getElementById("txtMaterno");
        var fechanac = document.getElementById("txtFechaNac");
        var sexo = document.getElementById("cbxSexo");
        var edocivil = document.getElementById("cbxEdoCivil");
        var domicilio = document.getElementById("txtDomicilio");
        var municipio = document.getElementById("txtMunicipio");
        var cp = document.getElementById("txtCP");
        var entfed = document.getElementById("txtEntidadFederativa");
        var nacionalidad = document.getElementById("txtNacionalidad");
        var celular = document.getElementById("txtTelCelular");
        var telfijo = document.getElementById("txtTelFijo");
        var mail = document.getElementById("txtMail");
        var idclinica = document.getElementById("cbxIdClinica");
        var rfc = document.getElementById("txtRFC");
        var titulo = document.getElementById("txtTitulo");
        var especialidad = document.getElementById("cbxEspecialidad");
        var cedula = document.getElementById("txtCedula");

    var array = new Array();

    array[0] = tipo;
    array[1] = nombre;
    array[2] = paterno;
    array[3] = materno;
    array[4] = fechanac;
    array[5] = sexo;
    array[6] = edocivil;
    array[7] = domicilio;
    array[8] = municipio;
    array[9] = cp;
    array[10] = entfed;
    array[11] = nacionalidad;
    array[12] = celular;
    array[13] = telfijo;
    array[14] = mail;
    array[15] = idclinica;
    array[16] = rfc;
    array[17] = titulo;
    array[18] = especialidad;
    array[19] = cedula;
    
    for (var i = 0; i <= 19; i++) {

        if ((i != 12)&&(i != 13)&&(i != 16)) {

            if (array[i].value.trim().length == 0) {
                $("#" + array[i].id).css("background", "#f0ad4e");
            } else {
                $("#" + array[i].id).css("background", "white");
            }

        }

    }

    if ((array[0].style.background).split(" ")[0] == "white" && (array[1].style.background).split(" ")[0] == "white" &&
            (array[2].style.background).split(" ")[0] == "white" && (array[3].style.background).split(" ")[0] == "white" &&
            (array[4].style.background).split(" ")[0] == "white" && (array[5].style.background).split(" ")[0] == "white" &&
            (array[6].style.background).split(" ")[0] == "white" && (array[7].style.background).split(" ")[0] == "white" &&
            (array[8].style.background).split(" ")[0] == "white" && (array[9].style.background).split(" ")[0] == "white" &&
            (array[10].style.background).split(" ")[0] == "white" && (array[11].style.background).split(" ")[0] == "white" &&
            (array[14].style.background).split(" ")[0] == "white" && (array[15].style.background).split(" ")[0] == "white" && 
            (array[17].style.background).split(" ")[0] == "white" && (array[18].style.background).split(" ")[0] == "white" && 
            (array[19].style.background).split(" ")[0] == "white") {

        var url = '../controladores/medicosCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=registrar' + '&tipo=' + array[0].value + '&nombre=' + array[1].value + '&apPaterno=' + array[2].value + '&apMaterno=' + array[3].value + '&fechaNac=' + array[4].value + '&sexo=' + array[5].value + '&edoCivil=' + array[6].value + '&domicilio=' + array[7].value + '&municipio=' + array[8].value + '&cp=' + array[9].value + '&entidadFederativa=' + array[10].value + '&nacionalidad=' + array[11].value + '&tel_celular=' + array[12].value + '&tel_fijo=' + array[13].value + '&mail=' + array[14].value + '&idClinica=' + array[15].value + '&rfc=' + array[16].value + '&titulo=' + array[17].value + '&idEspecialidad=' + array[18].value + '&cedula=' + array[19].value,
                success: function (data) {
                if (data == 'Registro exitoso.') {
                $('#confirmaciones').html(data);
                        window.setTimeout("paginacionMedicos(1);", 3000);
                } else {
                $('#confirmaciones').html(data);
                }
                }
        });

    } else {
        $('#confirmaciones').html("Verifique los campos marcados de color.");
    }

}

function eliminarPersonal(id){
        var url = '../controladores/medicosCRUD.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'accion=eliminar' + '&idPersonal=' + id,
            success: function (data) {
                if(data=="Eliminación exitosa."){
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-success"><h1>' + data + '</h1></div>');
                window.setTimeout("paginacionMedicos(1)",2000);
            }else{
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-danger"><h1>El registro no puede ser eliminado.</h1></div>');
                window.setTimeout("paginacionMedicos(1)",2000);
            }
            }
        });
    }

function consultarPersonal(id) {

    var url = '../controladores/medicosCRUD.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'accion=consultar' + '&idPersonal=' + id,
        success: function (data) {
            var array = data.split('|');
            $('#workArea').html(array[0]);
            $("#paginas").html("");
            seleccionarClinica(array[1]);
            seleccionarEspecialidad(array[2]);
        }
    });
}

function seleccionarEspecialidad(valor) {
    window.setTimeout("document.getElementById('cbxEspecialidad').value = '"+valor+"'.toString();",1000);
}

function modificarPersonal(){

var id = document.getElementById('txtID').value;

var tipo = document.getElementById("cbxTipoPersonal");
        var nombre = document.getElementById("txtNombre");
        var paterno = document.getElementById("txtPaterno");
        var materno = document.getElementById("txtMaterno");
        var fechanac = document.getElementById("txtFechaNac");
        var sexo = document.getElementById("cbxSexo");
        var edocivil = document.getElementById("cbxEdoCivil");
        var domicilio = document.getElementById("txtDomicilio");
        var municipio = document.getElementById("txtMunicipio");
        var cp = document.getElementById("txtCP");
        var entfed = document.getElementById("txtEntidadFederativa");
        var nacionalidad = document.getElementById("txtNacionalidad");
        var celular = document.getElementById("txtTelCelular");
        var telfijo = document.getElementById("txtTelFijo");
        var mail = document.getElementById("txtMail");
        var idclinica = document.getElementById("cbxIdClinica");
        var rfc = document.getElementById("txtRFC");
        var titulo = document.getElementById("txtTitulo");
        var especialidad = document.getElementById("cbxEspecialidad");
        var cedula = document.getElementById("txtCedula");

    var array = new Array();

    array[0] = tipo;
    array[1] = nombre;
    array[2] = paterno;
    array[3] = materno;
    array[4] = fechanac;
    array[5] = sexo;
    array[6] = edocivil;
    array[7] = domicilio;
    array[8] = municipio;
    array[9] = cp;
    array[10] = entfed;
    array[11] = nacionalidad;
    array[12] = celular;
    array[13] = telfijo;
    array[14] = mail;
    array[15] = idclinica;
    array[16] = rfc;
    array[17] = titulo;
    array[18] = especialidad;
    array[19] = cedula;
    
    for (var i = 0; i <= 19; i++) {

        if ((i != 12)&&(i != 13)&&(i != 16)) {

            if (array[i].value.trim().length == 0) {
                $("#" + array[i].id).css("background", "#f0ad4e");
            } else {
                $("#" + array[i].id).css("background", "white");
            }

        }

    }

    if ((array[0].style.background).split(" ")[0] == "white" && (array[1].style.background).split(" ")[0] == "white" &&
            (array[2].style.background).split(" ")[0] == "white" && (array[3].style.background).split(" ")[0] == "white" &&
            (array[4].style.background).split(" ")[0] == "white" && (array[5].style.background).split(" ")[0] == "white" &&
            (array[6].style.background).split(" ")[0] == "white" && (array[7].style.background).split(" ")[0] == "white" &&
            (array[8].style.background).split(" ")[0] == "white" && (array[9].style.background).split(" ")[0] == "white" &&
            (array[10].style.background).split(" ")[0] == "white" && (array[11].style.background).split(" ")[0] == "white" &&
            (array[14].style.background).split(" ")[0] == "white" && (array[15].style.background).split(" ")[0] == "white" && 
            (array[17].style.background).split(" ")[0] == "white" && (array[18].style.background).split(" ")[0] == "white" && 
            (array[19].style.background).split(" ")[0] == "white") {

var url = '../controladores/medicosCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=modificar' + '&idPersonal=' + id + '&tipo=' + array[0].value + '&nombre=' + array[1].value + '&apPaterno=' + array[2].value + '&apMaterno=' + array[3].value + '&fechaNac=' + array[4].value + '&sexo=' + array[5].value + '&edoCivil=' + array[6].value + '&domicilio=' + array[7].value + '&municipio=' + array[8].value + '&cp=' + array[9].value + '&entidadFederativa=' + array[10].value + '&nacionalidad=' + array[11].value + '&tel_celular=' + array[12].value + '&tel_fijo=' + array[13].value + '&mail=' + array[14].value + '&idClinica=' + array[15].value + '&rfc=' + array[16].value + '&titulo=' + array[17].value + '&idEspecialidad=' + array[18].value + '&cedula=' + array[19].value,
                success: function (data) {
                if (data == 'Modificación exitosa.') {
                $('#confirmaciones').html(data);
                        window.setTimeout("paginacionMedicos(1);", 3000);
                } else {
                $('#confirmaciones').html(data);
                }
                }
        });

    } else {
        $('#confirmaciones').html("Verifique los campos marcados de color.");
    }

}