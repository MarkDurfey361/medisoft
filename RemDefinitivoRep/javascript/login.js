var usuario = "";
var contrasena = "";

function limpiarNotificacion() {
    $('#notificacion').html('');
}

function RecogerCredenciales() {

    usuario = document.getElementById('nombreUsuario').value;
    contrasena = document.getElementById('contrasenaUsuario').value;

    if (usuario.length != 0 && contrasena != 0) {
        return true;
    } else {
        return false;
    }

}

function EnviarDatos() {

    if (RecogerCredenciales()) {

        $.ajax({
            type: 'POST',
            url: '../controladores/cLogin.php',
            data: ('usuario=' + usuario + '&contrasena=' + contrasena),
            success: function (confirmacion) {
                $('#notificacion').html('<h4 class="alert-danger text-center">' + confirmacion + '.</h>');
            }
        });

    } else {
        $('#notificacion').html('<h4 class="alert-warning text-center">Usuario y contraseña son necesarios.</h>');
    }

}
;

function Enter(e) {
    var codigo = e.keyCode;

    if (codigo === 13) {
        EnviarDatos();
    }

}