
function paginacionPacientes(index){
	var url = '../controladores/paginarPacientes.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'index='+index,
		success:function(data){
			var array = data.split('|');
			$('#workArea').html(array[0]);
			$('#paginas').html('<label style="margin-left: 20px; margin-top: 5px;" class="alert-info">No. de Página</label>' + array[1]);
		}
	});
	return false;
}

function paginacionCitas(index){
	var url = '../controladores/paginarCitas.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'index='+index,
		success:function(data){
			var array = data.split('|');
			$('#workArea').html(array[0]);
			$('#paginas').html('<label style="margin-left: 20px; margin-top: 5px;" class="alert-info">No. de Página</label>' + array[1]);
		}
	});
	return false;
}

function paginacionMedicos(index){
	var url = '../controladores/paginarMedicos.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'index='+index,
		success:function(data){
			var array = data.split('|');
			$('#workArea').html(array[0]);
			$('#paginas').html('<label style="margin-left: 20px; margin-top: 5px;" class="alert-info">No. de Página</label>' + array[1]);
		}
	});
	return false;
}

function paginacionInstrumental(index){
	var url = '../controladores/paginarInstrumental.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'index='+index,
		success:function(data){
			var array = data.split('|');
			$('#workArea').html(array[0]);
			$('#paginas').html('<label style="margin-left: 20px; margin-top: 5px;" class="alert-info">No. de Página</label>' + array[1]);
		}
	});
	return false;
}

function paginacionUsuarios(index){
	var url = '../controladores/paginarUsuarios.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'index='+index,
		success:function(data){
			var array = data.split('|');
			$('#workArea').html(array[0]);
			$('#paginas').html('<label style="margin-left: 20px; margin-top: 5px;" class="alert-info">No. de Página</label>' + array[1]);
		}
	});
	return false;
}

function paginacionQRCodigos(index){
	var url = '../controladores/paginarQRCodigos.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'index='+index,
		success:function(data){
			var array = data.split('|');
			$('#workArea').html(array[0]);
			$('#paginas').html('<label style="margin-left: 20px; margin-top: 5px;" class="alert-info">No. de Página</label>' + array[1]);
		}
	});
	return false;
}

function paginacionHistorias(index){
	var url = '../controladores/paginarHistorias.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'index='+index,
		success:function(data){
			var array = data.split('|');
			$('#workArea').html(array[0]);
			$('#paginas').html('<label style="margin-left: 20px; margin-top: 5px;" class="alert-info">No. de Página</label>' + array[1]);
		}
	});
	return false;
}
