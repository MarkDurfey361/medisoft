function formNuevoInstrumental(){

var form = '</fieldset><h3 class="text-center text-uppercase">Nuevo Instrumento</h3>\n\
                <center><form method="POST" class="form-inline" id="formNuevo">\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos de Registro</legend>\n\
                        <label class="etiqueta">ID/Folio:</label><input onkeyPress="return soloNumeros(event);" style="width: 100px;" id="txtID" type="text" class="form-control" placeholder="ID Instrumento">\n\
                        </fieldset>\n\
                        <fieldset>\n\
                            <legend class="text-uppercase alert-info text-info">Datos del Instrumento</legend>\n\
                        <label class="etiqueta">Nombre:</label><input onkeypress="return soloLetras(event);" id="txtNombre" type="text" class="form-control" placeholder="Nombre">\n\
                        <label class="etiqueta">Existencia:</label><input onkeypress="return soloNumeros(event);" id="txtExistencia" type="text" class="form-control" placeholder="Existencia">\n\
                        </fieldset>\n\
                        <fieldset>\n\
                        <legend id="confirmaciones"></legend>\n\
                        <input onclick="validarRegistro();" type="button" class="btn btn-success" value="Terminado">\n\
                        <input onclick="paginacionInstrumental(1);" type="button" class="btn btn-success" value="Cancelar">\n\
                    </fieldset></center>\n\
                </fieldset>';
        $("#workArea").html(form);
        $("#paginas").html("");
}

function validarRegistro() {

var array = [2];
        array[0] = document.getElementById("txtNombre").id;
        array[1] = document.getElementById("txtExistencia").id;
        if (validarControles(array[0]) == true){
if (validarControles(array[1]) == true){
var url = '../controladores/instrumentalCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=registrar' + '&nombre=' + document.getElementById(array[0]).value + '&existencia=' + document.getElementById(array[1]).value,
                success: function (data) {
                if (data == 'Registro exitoso.') {
                $('#confirmaciones').html(data);
                        window.setTimeout("paginacionInstrumental(1);", 3000);
                } else {
                $('#confirmaciones').html(data);
                }
                }
        });
        } else{
            $('#confirmaciones').html("Imposible Continuar, verifique la existencia ingresada");
        }
} else{
    $('#confirmaciones').html("Imposible Continuar, verifique el nombre");

}
}


function eliminarInstrumento(id){
        var url = '../controladores/instrumentalCRUD.php';
        $.ajax({
            type: 'POST',
            url: url,
            data: 'accion=eliminar' + '&idInstrumento=' + id,
            success: function (data) {
                if(data=="Eliminación exitosa."){
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-success"><h1>' + data + '</h1></div>');
                window.setTimeout("paginacionInstrumental(1)",2000);
            }else{
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-danger"><h1>El registro no puede ser eliminado.</h1></div>');
                window.setTimeout("paginacionInstrumental(1)",2000);
            }
            }
        });
    }

function consultarInstrumental(id) {

    var url = '../controladores/instrumentalCRUD.php';
    $.ajax({
        type: 'POST',
        url: url,
        data: 'accion=consultar' + '&idInstrumental=' + id,
        success: function (data) {
            $('#workArea').html(data);
            $("#paginas").html("");
        }
    });
}

function modificarRegistro() {

var id = document.getElementById('txtID').value;

var array = [2];
        array[0] = document.getElementById("txtNombre").id;
        array[1] = document.getElementById("txtExistencia").id;
        if (validarControles(array[0]) == true){
if (validarControles(array[1]) == true){
var url = '../controladores/instrumentalCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=modificar' + '&idInstrumental=' + id + '&nombre=' + document.getElementById(array[0]).value + '&existencia=' + document.getElementById(array[1]).value,
                success: function (data) {
                if (data == 'Modificación exitosa.') {
                $('#confirmaciones').html(data);
                        window.setTimeout("paginacionInstrumental(1);", 3000);
                } else {
                $('#confirmaciones').html(data);
                }
                }
        });
        } else{
            $('#confirmaciones').html("Imposible Continuar, verifique la existencia ingresada");
        }
} else{
    $('#confirmaciones').html("Imposible Continuar, verifique el nombre");

}
}