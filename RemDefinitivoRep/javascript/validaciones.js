function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46-9";

    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales.split("-")[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function soloNumeros(e)
{
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "1234567890";
    especiales = "8-37-39-46-9";

    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales.split("-")[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function soloMails(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " abcdefghijklmnñopqrstuvwxyz_@1234567890.";
    especiales = "8-37-39-46-9";

    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales.split("-")[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function soloDirecciones(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " abcdefghijklmnñopqrstuvwxyz1234567890#";
    especiales = "8-37-39-46-9";

    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales.split("-")[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function alfaNumericos(){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890";
    especiales = "8-37-39-46-9";

    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales.split("-")[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}