function formularioQR(){
    
var form = '</fieldset><h3 class="text-center text-uppercase">Nuevo QR</h3>\n\
                <center><form method="POST" class="form-inline" id="formNuevo">\n\
                        <fieldset>\n\
                            <legend class="text-uppercase text-info">Datos de Registro</legend>\n\
                        <input onkeyPress="return soloNumeros(event);" style="width: 100px;" id="txtID" type="text" class="form-control" placeholder="ID QR">\n\
                        <select id="cbxPSQR" class="btn form-control form-horizontal"></select>\n\
                        </fieldset>\n\
                        <fieldset>\n\
                        <legend id="confirmaciones"></legend>\n\
                        <input onclick="nuevoQR();" type="button" class="btn btn-success" value="Generar">\n\
                        <input onclick="paginacionQRCodigos(1)" type="button" class="btn btn-success" value="Cancelar">\n\
                    </fieldset></center>\n\
                <script>\n\
                    getPSQR("cbxPSQR");\n\
                </script>\n\
                </fieldset>';
        $("#workArea").html(form);
        $("#paginas").html("");
}

function getPSQR(control){

var url = '../controladores/PSQR.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: '',
                success: function (data) {
                $("#" + control).html(data);
                }
        });
}

function nuevoQR(){
    
    var array=[1];
        array[0] = document.getElementById("cbxPSQR").id;
        if(validarControles(array[0])==true){
            var url = '../controladores/generarQR.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: '&idPSQR=' + document.getElementById(array[0]).value,
                success: function (data) {
                    $('#confirmaciones').html(data);
                    window.setTimeout("paginacionQRCodigos(1);", 3000);
                }
        });
        }else{
            $('#confirmaciones').html("Imposible Generar, no seleccionó a ningún paciente");
        }
}

function eliminarQR(id){
var url = '../controladores/qrCRUD.php';
        $.ajax({
        type: 'POST',
                url: url,
                data: 'accion=eliminar' + '&idPaciente=' + id,
                success: function (data) {
                if(data=="Eliminación exitosa."){
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-success"><h1>' + data + '</h1></div>');
                window.setTimeout("paginacionQRCodigos(1)",2000);
            }else{
                $('#paginas').html('<div style="margin: 0 auto; width: 700px;" class="alert alert-danger"><h1>El registro no puede ser eliminado.</h1></div>');
                window.setTimeout("paginacionQRCodigos(1)",2000);
            }
            }
        });
}

function getComplemento(id){
    var url = '../controladores/getComplemento.php';
    var valor = "";    
    $.ajax({
        type: 'POST',
                url: url,
                data: 'idPaciente=' + id,
                success: function (data) {
                    incrustarModal(id,data)
                }
        });
        return valor;
}