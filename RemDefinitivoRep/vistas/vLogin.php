<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bienvenido a MediSoft</title>
        
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/misEstilos/login.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../jquery/jq.js"></script>
        <script type="text/javascript" src="../javascript/login.js"></script>
    </head>
    <body class="jumbotron">
        
        <div class="contentForm">
            
            <img class="img-responsive" width="100%" 
                 src="../resources/logoMedisoft.png" ><br>
            <fieldset method="POST">
                <input type="text" onkeypress="Enter(event);" 
                       id="nombreUsuario" onclick="limpiarNotificacion();" 
                       placeholder="Usuario" class="form-control cajaTexto"><br>
                <input type="password" onkeypress="Enter(event);" 
                       id="contrasenaUsuario" onclick="limpiarNotificacion();" 
                       placeholder="Contraseña" class="form-control cajaTexto"><br>
            </fieldset>
            <div class="btn-group botones">
                <input type="button" style="width: 130px; height: 50px; " 
                       onclick="EnviarDatos();" class="btn btn-success" 
                       value="Iniciar">
                <input type="button" style="width: 170px; height: 50px; " 
                       onclick="window.location='index.html'" class="btn btn-warning" 
                       value="Volver a la Web">
            </div>
            <div id="notificacion"></div>
            
        </div>
    </body>
</html>
