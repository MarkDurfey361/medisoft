<?php
include '../conexionBD/conexion.php';
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    
    
} else {
    ?>
    <script type="text/javascript">
        alert('Debe iniciar sesión');
        window.location = '../vistas/vLogin.php';
    </script>
    <?php
}
?>
    
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8" >
        <title>Panel Recepcionista</title>

        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/misEstilos/panelAdmin.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../jquery/jq.js"></script>
        <script type="text/javascript" src="../javascript/panelAdmin.js"></script>
        <script type="text/javascript" src="../javascript/paciente.js"></script>
        <script type="text/javascript" src="../javascript/historiasClinicas.js"></script>
        <script type="text/javascript" src="../javascript/qr.js"></script>
        <script type="text/javascript" src="../javascript/usuario.js"></script>
        <script type="text/javascript" src="../javascript/medico.js"></script>
        <script type="text/javascript" src="../javascript/mensajes.js"></script>
        <script type="text/javascript" src="../javascript/instrumental.js"></script>
        <script type="text/javascript" src="../javascript/paginacion.js"></script>
        <script type="text/javascript" src="../javascript/validaciones.js"></script>
        <link href="../css/jquery-ui.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../javascript/jquery-ui.js"></script>
        <script type="text/javascript" src="../javascript/citas.js"></script>
        
        <script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
        <script type="text/javascript" src="../javascript/bootstrap.min_1.js"></script>
        
    </head>
    <body>
        <div class="barraSuperior">
            <center><h2 id="tipo" style="color: white;">Recepcionista</h2></center>
        </div>
        <div id="menuLateral">
            <div id="primerContenedor">
                <center><img id="fotoPersonal" class="img-responsive" width="90%" height="90%" src="../resources/log.jpg"></center>
            </div>
            
            <div id="segundoContenedor" class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="alert('mostrarSuInfo');" value="Conectado" src="../resources/conectado.png"></div>
                <p onclick="misDatos('mostrarSuInfo');" id="Conectado"  class="texto"><?php echo $_SESSION['username'] ?></p>    
            </div>
            
            <!--<div class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionPacientes(1);" value="Pacientes" src="../resources/pacienteIcono.png"></div>
                <p onclick="paginacionPacientes(1);" id="Pacientes"  class="texto">Pacientes</p>    
            </div>
            
            <div class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionCitas(1);" value="Citas" src="../resources/citasIcono.png"></div>
                <p onclick="paginacionCitas(1);" id="Citas" class="texto">Citas</p>    
            </div>
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionQR(1);" value="QRCodes" src="../resources/QRCode.png"></div>
                <p onclick="paginacionQRCodigos(1);" id="QRCodes"  class="texto">QR Codigos</p>    
            </div>-->
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="" value="QRCodes" src="../resources/turnoIcono.png"></div>
                <p onclick="formTurnos();" id="QRCodes"  class="texto">Nuevo Turno</p>
            </div>
            
            <!--
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="mostrarTareas();" value="Nuevo..." src="../resources/nuevoIcono.png"></div>
                <p onclick="mostrarTareas();" id="Nuevo"  class="texto">Nueva Tarea...</p>    
            </div>
            -->
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="mensajeCerrarSesion();" value="Cerrar Sesion" src="../resources/cerrar.png"></div>
                <p onclick="mensajeCerrarSesion();" id="CerrarSesion"  class="texto">Salir</p>    
            </div>
        </div>
        
        <div class="sobrante">
            <table id="zonaBusqueda">
                <tr>
                    <td colspan="2"><label style="margin-left: 30px;">Buscar por:</label></td>
                </tr>
                <tr>
                    <td><div style="margin-left: 30px;" id="cajaDeBusqueda">
                            <input onkeypress="return soloNumeros(event);" id='buscar' type='text' class='form-control cajaBusqueda' placeholder='Búsqueda General' onkeyup='busquedaGeneral(this.id);'>
            </div></td>
                </tr>
            </table>
            
            <div id="workArea" class="workArea"></div>
            
            <center><form class="form-inline">
                <fieldset>
                    
                    <legend class="text-info alert-info text-uppercase">Siguiente Turno</legend>
                    <label class="etiqueta">Siguiente: </label><br>
                    <input readonly="" class="form-control" value="1" type="text" id="txtTurnoSiguiente">
                    
                </fieldset>
                <fieldset>
                    
                    <legend class="text-info alert-info text-uppercase">Imprimir:</legend>
                    
                    <input type="button" class="btn btn-info" onclick="turno();" value="Imprimir Nuevo Turno"><br><br>
                    </fieldset>
                    
                    <fieldset>
                        <legend class="text-info alert-info text-uppercase">Último Impreso</legend>
                        <center><div id="divTurno"><div id="divInterno" style="border-radius: 19px 19px 19px 19px; border: solid 1px black; width: 300px;">
                            <table>
                                <tr><h2>POR FAVOR, ESPERE A SER ATENDIDO</h2></tr>
                                <tr><h3>GRACIAS</h3></tr>
                                <tr><h4>SU TURNO ES EL:</h4></tr>
                                <tr><h1 id="turno"></h1></tr>
                            </table>
                    </div></div></center>
                    
                </fieldset>
            </form></center>
            
            <center>
                    <ul class="pagination" id="paginas"></ul>
            </center>
        </div>
        
        <div id="moda">
            
        </div>
        
        <script>
        
        var turnoActual = 1;
        
        function turno(){
            document.getElementById("turno").innerHTML=turnoActual;
            turnoActual++;
            document.getElementById("txtTurnoSiguiente").value=turnoActual;
            
            imprimirTurno(document.getElementById("divTurno").id);
            
        }
        
        
        function imprimirTurno(nombreDiv) {
    var objeto=document.getElementById(nombreDiv);  //obtenemos el objeto a imprimir
    var ventana=window.open('','_blank');  //abrimos una ventana vacía nueva
    ventana.document.write('<head><title>Imprimir Turno</title><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><style>*{font-family: sans-serif;} div{border: solid 2px black; width: 300px;}</style></head>');
    ventana.document.write('<body><center>');
    ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
    ventana.document.write('</body></center>');
    ventana.document.close();  //cerramos el documento
    ventana.print();  //imprimimos la ventana
    ventana.close();  //cerramos la ventana
 }
        
        /*
        function nuevoTurno(){
            document.getElementById("txtTurnoActual").value = array[turnoActual];
            array[turnoActual] = turnoActual;
            document.getElementById("txtTurnoSiguiente").value = turnoActual+1;
            turnoActual++;
            alert(array);
        }
        
        function liberarActual(){
            document.getElementById("txtTurnoActual").value = array[turnoActual];
            array[turnoActual] = "liberado";
        }
        */
        </script>
        
    </body>
</html>