<?php
include '../conexionBD/conexion.php';
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    
    if(isset($_SESSION['tipo']) && $_SESSION['tipo'] != "Administrador"){
        ?>
        <script type="text/javascript">
            alert('No tiene permiso para este sitio');
            window.location = '../vistas/vLogin.php';
        </script>
        <?php    
    }
    
} else {
    ?>
    <script type="text/javascript">
        alert('Debe iniciar sesión');
        window.location = '../vistas/vLogin.php';
    </script>
    <?php
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8" >
        <title>Panel Administrador</title>

        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/misEstilos/panelAdmin.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../jquery/jq.js"></script>
        <script type="text/javascript" src="../javascript/panelAdmin.js"></script>
        <script type="text/javascript" src="../javascript/paciente.js"></script>
        <script type="text/javascript" src="../javascript/historiasClinicas.js"></script>
        <script type="text/javascript" src="../javascript/qr.js"></script>
        <script type="text/javascript" src="../javascript/usuario.js"></script>
        <script type="text/javascript" src="../javascript/medico.js"></script>
        <script type="text/javascript" src="../javascript/instrumental.js"></script>
        <script type="text/javascript" src="../javascript/paginacion.js"></script>
        <script type="text/javascript" src="../javascript/validaciones.js"></script>
        <link href="../css/jquery-ui.css" rel="stylesheet" media="screen">        
        <script type="text/javascript" src="../javascript/jquery-ui.js"></script>
        <script type="text/javascript" src="../javascript/mensajes.js"></script>
        <script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
        <script type="text/javascript" src="../javascript/bootstrap.min_1.js"></script>
        
    </head>
    <body>
        <div class="barraSuperior">
            <center><h2 id="tipo">Administrador</h2></center>
        </div>
        <div id="menuLateral">
            <div onclick="formFotoPerfil();" id="primerContenedor">
                <center><img id="fotoPersonal" class="img-responsive" width="90%" height="90%" src="../resources/log.jpg"></center>
            </div>
            
            <div id="segundoContenedor" class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="alert('mostrarSuInfo');" value="Conectado" src="../resources/conectado.png"></div>
                <p onclick="misDatos('mostrarSuInfo');" id="Conectado"  class="texto"><?php echo $_SESSION['username'] ?></p>    
            </div>
            
            <div class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionPacientes(1);" value="Pacientes" src="../resources/pacienteIcono.png"></div>
                <p onclick="paginacionPacientes(1);" id="Pacientes"  class="texto">Pacientes</p>    
            </div>

            <div class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionPacientes(1);" value="Pacientes" src="../resources/medicoIcono.png"></div>
                <p onclick="paginacionMedicos(1);" id="Medicos"  class="texto">Personal</p>    
            </div>
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionInstrumental(1);" value="Instrumental" src="../resources/citasIcono.png"></div>
                <p onclick="paginacionInstrumental(1);" id="Instrumental"  class="texto">Instrumental</p>    
            </div>

            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionUsuarios(1);" value="Usuarios" src="../resources/usuariosIcono.png"></div>
                <p onclick="paginacionUsuarios(1);" id="Usuarios"  class="texto">Usuarios</p>    
            </div>

            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionQR(1);" value="QRCodes" src="../resources/QRCode.png"></div>
                <p onclick="paginacionQRCodigos(1);" id="QRCodes"  class="texto">QR Codigos</p>    
            </div>
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionHistorias(1);" value="Historias" src="../resources/historiaIcono.png"></div>
                <p onclick="paginacionHistorias(1);" id="QRCodes"  class="texto">Historias C</p>
            </div>
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="mensajeCerrarSesion();" value="Cerrar Sesion" src="../resources/cerrar.png"></div>
                <p onclick="mensajeCerrarSesion();" id="CerrarSesion"  class="texto">Salir</p>    
            </div>
        </div>
        
        <div class="sobrante">
            <table id="zonaBusqueda">
                <tr>
                    <td colspan="2"><label class="etiqueta" style="margin-top: 3px; margin-left: 35px;">Busqueda manual:</label></td>
                </tr>
                <tr>
                    <td><div id="cajaDeBusqueda">
                            <center><input id="buscar" type="text" class="form-control cajaBusqueda" placeholder="Búsqueda General" onkeyup="busquedaGeneral(this.id);"></center>
            </div></td>
                </tr>
            </table>
            
            <div id="workArea" class="workArea"></div>
            <center>
                    <ul class="pagination" id="paginas"></ul>
            </center>
        </div>
        
        <div id="moda">
            
        </div>
        
    </body>
</html>