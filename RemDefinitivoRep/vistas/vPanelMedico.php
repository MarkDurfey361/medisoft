<?php
include '../conexionBD/conexion.php';
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    
    if(isset($_SESSION['tipo']) && $_SESSION['tipo'] != "Medico"){
        ?>
        <script type="text/javascript">
            alert('No tiene permiso para este sitio');
            window.location = '../vistas/vLogin.php';
        </script>
        <?php    
    }

} else {
    ?>
    <script type="text/javascript">
        alert('Debe iniciar sesión');
        window.location = '../vistas/vLogin.php';
    </script>
    <?php
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8" >
        <title>Panel Médico</title>

        <script type="text/javascript" src="../javascript/certificados.js"></script>
        
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../css/misEstilos/panelAdmin.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../jquery/jq.js"></script>
        <script type="text/javascript" src="../javascript/panelAdmin.js"></script>
        <script type="text/javascript" src="../javascript/paciente.js"></script>
        <script type="text/javascript" src="../javascript/historiasClinicas.js"></script>
        <script type="text/javascript" src="../javascript/qr.js"></script>
        <script type="text/javascript" src="../javascript/usuario.js"></script>
        <script type="text/javascript" src="../javascript/medico.js"></script>
        <script type="text/javascript" src="../javascript/mensajes.js"></script>
        <script type="text/javascript" src="../javascript/instrumental.js"></script>
        <script type="text/javascript" src="../javascript/paginacion.js"></script>
        <script type="text/javascript" src="../javascript/validaciones.js"></script>
        <link href="../css/jquery-ui.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../javascript/jquery-ui.js"></script>
        <script type="text/javascript" src="../javascript/citas.js"></script>
        
        
        <script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
        <script type="text/javascript" src="../javascript/bootstrap.min_1.js"></script>
        
        
        
    </head>
    <body>
        <div class="barraSuperior">
            <center><h2 id="tipo" style="color: white;">Médico</h2></center>
        </div>
        <div id="menuLateral">
            <div id="primerContenedor">
                <center><img id="fotoPersonal" class="img-responsive" width="90%" height="90%" src="../resources/log.jpg"></center>
            </div>
            
            <div id="segundoContenedor" class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="alert('mostrarSuInfo');" value="Conectado" src="../resources/conectado.png"></div>
                <p onclick="" id="Conectado"  class="texto"><?php echo $_SESSION['username'] ?></p>    
            </div>
            
            <div class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionPacientes(1);" value="Pacientes" src="../resources/pacienteIcono.png"></div>
                <p onclick="paginacionPacientes(1);" id="Pacientes"  class="texto">Pacientes</p>    
            </div>
            
            <div class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionCitas(1);" value="Citas" src="../resources/citasIcono.png"></div>
                <p onclick="paginacionCitas(1);" id="Citas" class="texto">Citas</p>    
            </div>
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="paginacionQRCodigos(1);" value="QRCodes" src="../resources/QRCode.png"></div>
                <p onclick="paginacionQRCodigos(1);" id="QRCodes"  class="texto">QR Codigos</p>    
            </div>
            
            <!--<div class="content">
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" value="Nuevo certificado" src="../resources/certificado.png"></div>
                <a href="descargarCertificado.php?historia=Certificado_Medico.docx"><p id="Citas" class="texto">Certificado</p></a>
            </div>-->
            
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="nuevoCertificado();" value="Nuevo..." src="../resources/nuevoIcono.png"></div>
                <p onclick="nuevoCertificado();" id="Nuevo"  class="texto">Certificado</p>    
            </div>
            
            
            <div class="content" >
                <div class="logo"><input type="image" class="img-rounded img-responsive" width="40px" height="40px" onclick="mensajeCerrarSesion();" value="Cerrar Sesion" src="../resources/cerrar.png"></div>
                <p onclick="mensajeCerrarSesion();" id="CerrarSesion"  class="texto">Salir</p>    
            </div>
        </div>
        
        <div class="sobrante">
            <table id="zonaBusqueda">
                <tr>
                    <td colspan="2"><label style="margin-left: 30px;">Buscar por:</label></td>
                </tr>
                <tr>
                    <td><div style="margin-left: 30px;" id="cajaDeBusqueda">
                            <input id='buscar' type='text' class='form-control cajaBusqueda' placeholder='Búsqueda General' onkeyup='busquedaGeneral(this.id);'>
            </div></td>
                </tr>
            </table>
            
            <div id="workArea" class="workArea"></div>
            <center>
                    <ul class="pagination" id="paginas"></ul>
            </center>
        </div>
        
        <div id="moda">
            
        </div>
        
    </body>
</html>