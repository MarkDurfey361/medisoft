<?php

//$host_db = "localhost";
//$user_db = "root";
//$pass_db = "";
//$db_name = "uni_rem";
//
//$instancia = new mysqli($host_db, $user_db, $pass_db, $db_name);
//
//if ($instancia->connect_error) {
//    die("La conexion falló: " . $instancia->connect_error);
//}
//
//function CerrarConexion($conexion){
//    mysqli_close($conexion);
//}

$DBhost = "localhost";
$DBuser = "root";
$DBpass = "";
$DBname = "uni_rem";

try {
    $DBcon = new PDO("mysql:host=$DBhost;dbname=$DBname", $DBuser, $DBpass);
    $DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $ex) {
    die($ex->getMessage());
}

//$query = "SELECT nombreUsuario, contrasenaUsuario FROM usuarios";
//
//$stmt = $DBcon->prepare($query);
//$stmt->execute();
//
//$userData = array();
//
//while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//
//    $userData["Usuarios"][] = $row;
//}
//
//echo json_encode($userData);
