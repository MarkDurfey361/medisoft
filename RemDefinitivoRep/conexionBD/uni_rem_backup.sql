-- MySQL dump 10.13  Distrib 5.5.8, for Win32 (x86)
--
-- Host: localhost    Database: uni_rem
-- ------------------------------------------------------
-- Server version	5.5.8-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `citas`
--

DROP TABLE IF EXISTS `citas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `citas` (
  `id_cita` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY (`id_cita`),
  KEY `id_paciente` (`id_paciente`),
  CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `pacientes` (`id_paciente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `citas`
--

LOCK TABLES `citas` WRITE;
/*!40000 ALTER TABLE `citas` DISABLE KEYS */;
/*!40000 ALTER TABLE `citas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinica`
--

DROP TABLE IF EXISTS `clinica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinica` (
  `id_clinica` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` longtext,
  `direccion` longtext NOT NULL,
  `tel_celular` varchar(15) DEFAULT NULL,
  `tel_fijo` varchar(15) NOT NULL,
  PRIMARY KEY (`id_clinica`),
  UNIQUE KEY `tel_celular` (`tel_celular`,`tel_fijo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinica`
--

LOCK TABLES `clinica` WRITE;
/*!40000 ALTER TABLE `clinica` DISABLE KEYS */;
INSERT INTO `clinica` VALUES (1,'Unidad Médica Virgen de los Remedios','Virgen de los Remedios MZ35 LT 5 C-B, Fraccionamiento la Guadalupana.','5562428516','62582060'),(2,'Unidad Médica Virgen de los Remedios','Av. Los sauces #14, Colonia Roma, Ciudad de México.','5511892964','53447619');
/*!40000 ALTER TABLE `clinica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialidades`
--

DROP TABLE IF EXISTS `especialidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especialidades` (
  `id_especialidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  PRIMARY KEY (`id_especialidad`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialidades`
--

LOCK TABLES `especialidades` WRITE;
/*!40000 ALTER TABLE `especialidades` DISABLE KEYS */;
INSERT INTO `especialidades` VALUES (1,'Anatomía Patológica'),(2,'Medicina General'),(3,'Anestesiología y Recuperación'),(4,'Medicina General'),(5,'Anestesiología Pediátrica'),(6,'Especialista en Anestesiología'),(7,'Cardiología'),(8,'Medicina General'),(9,'Cardiología Pediátrica'),(10,'Especialista en Pediatría'),(11,'Cirugía General'),(12,'Cirugía Pediátrica'),(13,'Cirugía Plástica y Reconstructiva'),(14,'Cirugía Torácica General'),(15,'Cirugía Cardiovascular Torácica'),(16,'Dermatología'),(17,'Endocrinología'),(18,'Gastroenterología'),(19,'Geriatría y Gerontología'),(20,'Ginecología y Obstetricia'),(21,'Ginecología Oncológica'),(22,'Hematología'),(23,'Hematología Pediátrica'),(24,'Infectología'),(25,'Infectología Pediátrica'),(26,'Inmunología Clínica Médica'),(27,'Medicina Crítica Pediátrica'),(28,'Medicina Crítica y Terapia Intensiva'),(29,'Medicina de Emergencias'),(30,'Medicina Interna'),(31,'Medicina Familiar y Comunitaria'),(32,'Medicina Física y Rehabilitación'),(33,'Medicina Materno Fetal'),(34,'Medicina Paliativa'),(35,'Nefrología'),(36,'Nefrología Pediátrica'),(37,'Neonatología'),(38,'Neumología'),(39,'Neumología Pediátrica'),(40,'Neurocirugía'),(41,'Neurología'),(42,'Oftalmología'),(43,'Oftalmología Pediátrica'),(44,'Oncología Médica'),(45,'Oncología Quirúrgica'),(46,'Ortopedia y Traumatología'),(47,'Ortopedia y Traumatología'),(48,'Otorrinolaringología'),(49,'Patología Pediátrica'),(50,'Pediatría'),(51,'Psicología Clínica'),(52,'Psiquiatría'),(53,'Radiología e Imágenes Médicas'),(54,'Radioterapia'),(55,'Reumatología'),(56,'Urología'),(57,'Urología Pediátrica'),(58,'Vascular Periférico');
/*!40000 ALTER TABLE `especialidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historia_clinica`
--

DROP TABLE IF EXISTS `historia_clinica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historia_clinica` (
  `id_historia` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) DEFAULT NULL,
  `fecha_subida` date NOT NULL,
  `comentarios` varchar(200) DEFAULT NULL,
  `archivo_historia` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_historia`),
  KEY `id_paciente` (`id_paciente`),
  CONSTRAINT `historia_clinica_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `pacientes` (`id_paciente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historia_clinica`
--

LOCK TABLES `historia_clinica` WRITE;
/*!40000 ALTER TABLE `historia_clinica` DISABLE KEYS */;
INSERT INTO `historia_clinica` VALUES (3,1027,'2017-08-16','','../HistoriasClinicas/categorias.docx'),(5,1028,'2017-08-16','Hola','../HistoriasClinicas/CV GUADALUPE PRADO.docx');
/*!40000 ALTER TABLE `historia_clinica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrumental`
--

DROP TABLE IF EXISTS `instrumental`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instrumental` (
  `id_instrumento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `existencia` int(11) NOT NULL DEFAULT '0',
  `fotografia` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_instrumento`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrumental`
--

LOCK TABLES `instrumental` WRITE;
/*!40000 ALTER TABLE `instrumental` DISABLE KEYS */;
INSERT INTO `instrumental` VALUES (4,'Tenazas',34,NULL),(5,'Mechero',65,NULL);
/*!40000 ALTER TABLE `instrumental` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrumentos`
--

DROP TABLE IF EXISTS `instrumentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instrumentos` (
  `id_instrumento` int(11) NOT NULL AUTO_INCREMENT,
  `id_clinica` int(11) DEFAULT NULL,
  `nombre_instrumento` varchar(50) NOT NULL,
  `cantidad_actual` int(4) NOT NULL DEFAULT '0',
  `fotografia` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_instrumento`),
  KEY `id_clinica` (`id_clinica`),
  CONSTRAINT `instrumentos_ibfk_1` FOREIGN KEY (`id_clinica`) REFERENCES `clinica` (`id_clinica`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrumentos`
--

LOCK TABLES `instrumentos` WRITE;
/*!40000 ALTER TABLE `instrumentos` DISABLE KEYS */;
INSERT INTO `instrumentos` VALUES (1,1,'Bisturí',50,NULL);
/*!40000 ALTER TABLE `instrumentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacientes`
--

DROP TABLE IF EXISTS `pacientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pacientes` (
  `id_paciente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `ap_paterno` varchar(30) NOT NULL,
  `ap_materno` varchar(30) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` varchar(8) NOT NULL,
  `edo_civil` varchar(25) NOT NULL,
  `domicilio` varchar(150) NOT NULL,
  `municipio` varchar(30) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `entidad_federativa` varchar(30) NOT NULL,
  `nacionalidad` varchar(30) NOT NULL,
  `tel_celular` varchar(10) DEFAULT NULL,
  `tel_fijo` varchar(8) NOT NULL,
  `mail` varchar(40) NOT NULL,
  `fotografia` varchar(150) DEFAULT NULL,
  `id_clinica` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_paciente`),
  UNIQUE KEY `mail` (`mail`),
  KEY `id_clinica` (`id_clinica`),
  CONSTRAINT `pacientes_ibfk_1` FOREIGN KEY (`id_clinica`) REFERENCES `clinica` (`id_clinica`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1031 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacientes`
--

LOCK TABLES `pacientes` WRITE;
/*!40000 ALTER TABLE `pacientes` DISABLE KEYS */;
INSERT INTO `pacientes` VALUES (1027,'Martín','Soza','Juárez','1987-04-02','Hombre','Casado','Super Manzana 24 #65','Acambay','77443','Pachuca','Mexicana','5538714625','59624418','julian@gmail.com',NULL,1),(1028,'Carlos','Valencia','Saltivar','1981-08-11','Hombre','Casado','La Monera #45','Zumpango','88336','Estado De México','Mexicana','5529043628','59628362','carlos@gmail.com',NULL,1),(1029,'Pancho','Pérez','Jolote','2000-01-01','Hombre','Soltero','Hata alla tecamac','Tecámac','246802','México','Mexicana','5556575859','55545352','pancho@gmail.com.mx.edu',NULL,2),(1030,'gdxgxd','bdrhf','hhiu','2017-11-14','Mujer','Viudo','86yhujkiujk','gfhghnjhb','345678','ftyhujio','fgyhujimko','345678','4567','sadasd',NULL,1);
/*!40000 ALTER TABLE `pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal`
--

DROP TABLE IF EXISTS `personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal` (
  `id_personal` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_personal` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `ap_paterno` varchar(30) NOT NULL,
  `ap_materno` varchar(30) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` varchar(8) NOT NULL,
  `edo_civil` varchar(25) NOT NULL,
  `domicilio` varchar(150) NOT NULL,
  `municipio` varchar(30) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `entidad_federativa` varchar(30) NOT NULL,
  `nacionalidad` varchar(30) NOT NULL,
  `tel_celular` varchar(30) DEFAULT NULL,
  `tel_fijo` varchar(30) DEFAULT NULL,
  `mail` varchar(50) NOT NULL,
  `rfc` varchar(50) DEFAULT NULL,
  `titulo` varchar(30) NOT NULL,
  `id_especialidad` int(11) NOT NULL,
  `cedula_profesional` varchar(10) NOT NULL,
  `id_clinica` int(11) DEFAULT NULL,
  `fotografia` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_personal`),
  UNIQUE KEY `tel_fijo` (`tel_fijo`,`tel_celular`,`rfc`,`mail`,`cedula_profesional`),
  KEY `id_clinica` (`id_clinica`),
  KEY `id_especialidad` (`id_especialidad`),
  CONSTRAINT `personal_ibfk_1` FOREIGN KEY (`id_clinica`) REFERENCES `clinica` (`id_clinica`),
  CONSTRAINT `personal_ibfk_2` FOREIGN KEY (`id_especialidad`) REFERENCES `especialidades` (`id_especialidad`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal`
--

LOCK TABLES `personal` WRITE;
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` VALUES (2,'Administrador','Guadalupe','Prado','Pérez','1980-08-13','Mujer','Casado','Av. Hidalgo #158, Santa María, Estado de México','Apaxco','55660','Estado de México','Mexicana','5519005991','66332277','lup_evanz.44@hotmail.com','GUDU800825569','Doctor',56,'9250663',1,'../Fotos De Perfil/10730842_882101931800481_3048310547265317907_n.jpg'),(3,'Medico','Leonardo Felipe','Prado','Pérez','1982-08-10','Hombre','Casado','Av. Hidalgo #158, Santa María, Estado de México','Apaxco','55660','Estado de México','Mexicana','5588776655','66000000','lup_evanz.44@hotmail.com','GUDU800825569','Doctor',56,'1234567',1,NULL),(7,'Recepcionista','Ricardo','Verne','Vega','1982-07-01','Hombre','Viudo','Av Los PInos','Ecatepec','55382','México','Mexicana','5599254145','53807278','ricardo@gmail.com','DEAR56766FH67','Técnico',1,'777688',1,NULL);
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qrcode`
--

DROP TABLE IF EXISTS `qrcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qrcode` (
  `id_qrcode` int(11) NOT NULL AUTO_INCREMENT,
  `id_paciente` int(11) DEFAULT NULL,
  `qr_imagen` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_qrcode`),
  UNIQUE KEY `qr_imagen` (`qr_imagen`),
  KEY `id_paciente` (`id_paciente`),
  CONSTRAINT `qrcode_ibfk_1` FOREIGN KEY (`id_paciente`) REFERENCES `pacientes` (`id_paciente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qrcode`
--

LOCK TABLES `qrcode` WRITE;
/*!40000 ALTER TABLE `qrcode` DISABLE KEYS */;
INSERT INTO `qrcode` VALUES (29,1027,'../QR/1027.png');
/*!40000 ALTER TABLE `qrcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_personal` int(11) DEFAULT NULL,
  `nombreUsuario` varchar(64) NOT NULL,
  `contrasenaUsuario` varchar(64) NOT NULL,
  `tipoUsuario` enum('Recepcionista','Enfermera','Medico','Almacenista','Administrador') DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `nombreUsuario` (`nombreUsuario`),
  KEY `id_personal` (`id_personal`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id_personal`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (2,2,'Lup Evanz','Ttl25nearOxO','Administrador'),(4,3,'Leon Leon','leonardo','Medico'),(6,7,'Rick','Ttl','Recepcionista');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-15 18:24:19
